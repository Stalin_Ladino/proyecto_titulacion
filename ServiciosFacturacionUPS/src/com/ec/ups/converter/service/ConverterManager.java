package com.ec.ups.converter.service;

import java.util.Collection;

import org.json.JSONArray;
import org.json.JSONException;

import com.ec.ups.service.data.ConvertFromServiceData;

public interface ConverterManager<T> {

	T convert(ConvertFromServiceData<?> converter) throws Exception;

	Collection<T> convertToCollection(ConvertFromServiceData<?> converter) throws Exception, JSONException, Exception;
	
	Collection<T> convertToCollectionType(ConvertFromServiceData<?> converter) throws Exception;

	ConvertFromServiceData<?> getConverter();

	JSONArray convertFromCollectionToJson(ConvertFromServiceData<?> converter, Collection<T> collection)
			throws JSONException;
	
	JSONArray convertFromCollectionToJsonCount(ConvertFromServiceData<?> converter, Collection<T> collection)
			throws JSONException;
	
	JSONArray convertFromCollectionToJsonType(ConvertFromServiceData<?> converter, Collection<T> collection)
			throws JSONException;

	@SuppressWarnings("rawtypes")
	JSONArray convertFromCollectionToJsonDateTime(
			ConvertFromServiceData converter, Collection collection)
			throws JSONException;
	
	@SuppressWarnings("rawtypes")
	JSONArray convertFromCollectionToJsonAccess(
			ConvertFromServiceData converter, Collection collection)
			throws JSONException;

}
