package com.ec.ups.service.data;

import java.util.Collection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ec.ups.jpa.entidades.secComDTO;
import com.gpf.util.web.ServicioUtil;

public class ConvertFromDataToSectorUps extends ConvertFromServiceData<secComDTO> {

	private static final ConvertFromDataToSectorUps INSTANCIA = new ConvertFromDataToSectorUps();

	public static ConvertFromDataToSectorUps getInstancia() {

		return INSTANCIA;
	}

	public static ConvertFromDataToSectorUps getInstancia(String jsonData) {

		INSTANCIA.setJsonData(jsonData);
		return INSTANCIA;
	}

	private ConvertFromDataToSectorUps() {

	}

	/**
	 * Permite crear un secComDTO a partir de una trama JSON enviada
	 * 
	 * @return secComDTO entidad JSON con los datos enviados en la trama json
	 * @throws JSONException,
	 *             en caso de que ocurra una excepcion
	 */
	public secComDTO convertToSession() throws JSONException {
		JSONObject jsonData = new JSONObject(this.jsonData);
		return ServicioUtil.getInstancia()
				.createSectorDTOInstance(ServicioUtil.getInstancia().convertJsonData(jsonData));

	}
	/**
	 * Permite crear un sectorDTO a partir de una trama JSON enviada
	 * @return
	 * @throws JSONException
	 */
	public secComDTO convertToSector() throws JSONException {
		JSONObject jsonData = new JSONObject(this.jsonData);
		return ServicioUtil.getInstancia()
				.createSectorDTOInstance(ServicioUtil.getInstancia().convertJsonData(jsonData));

	}

	@Override
	public secComDTO convert() throws Exception {
		try {
			return convertToSector();
		} catch (Exception e) {
//			e.printStackTrace();
			//throw new Exception(MensajesUtil.getString("mensagge.error.estructure.json"),ResultEnum.ERROR.getValor());
		}
		return null;
	}
	@Override
	public secComDTO convertSector() throws Exception {
		try {
			return convertToSector();
		} catch (Exception e) {
//			e.printStackTrace();
			//throw new Exception(MensajesUtil.getString("mensagge.error.estructure.json"),ResultEnum.ERROR.getValor());
		}
		return null;
	}
	
	@Override
	public Collection<secComDTO> convertToCollection() throws Exception {
		return null;
	}

	/**
	 * Permite generar una trama json a partir de una entidad Session
	 * 
	 * @param session,
	 *            entidad que se desea parsear a trama json
	 * @return JsonObject, es la trama json que respresenta a la entidad session
	 * @throws JSONException,
	 *             en caso de que exista una excepcion
	 */
	private JSONObject createLoginData(secComDTO sec) throws JSONException {
		JSONObject sessionJSON = new JSONObject();
		sessionJSON.put("id", sec.getIdSec());
		sessionJSON.put("nombre", sec.getNomSec());
		return sessionJSON;
	}

	@Override
	public JSONArray convertFromCollectionToJson(Collection<secComDTO> collection) throws JSONException {
		JSONArray sessionArray = new JSONArray();
		if (collection != null && !collection.isEmpty()) {
			for (secComDTO session : collection) {
				sessionArray.put(createLoginData(session));
			}
		}
		return sessionArray;
	}


	
}
