package com.ec.ups.servicios.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.core.Response;

import com.ec.ups.converter.service.ConverterMangerImpl;
import com.ec.ups.ejb.servicios.facturacionServiceEJB;
import com.ec.ups.jpa.entidades.TblConsumo;
import com.ec.ups.jpa.entidades.TblSectorUsuario;
import com.ec.ups.jpa.entidades.loginDTO;
import com.ec.ups.jpa.entidades.secComDTO;
import com.ec.ups.jpa.entidades.usuarioDTO;
import com.ec.ups.service.data.ConvertFromDataToConsumoUps;
import com.ec.ups.service.data.ConvertFromDataToLoginUps;
import com.ec.ups.service.data.ConvertFromDataToSectorUps;
import com.ec.ups.service.estructura.responseEstructure;
import com.ec.ups.servicios.web.facturacionService;
import com.gpf.util.web.ServicioUtil;

@Stateless
public class facturacionServiceImpl implements facturacionService {

	@EJB
	private facturacionServiceEJB facturacionServiceEJB;
	
	private ConverterMangerImpl<loginDTO> converterManager = new ConverterMangerImpl<loginDTO>();
	
	private ConverterMangerImpl<TblConsumo> converterManagerConsumo = new ConverterMangerImpl<TblConsumo>();
	
	@Override
	public Response loginUps(String loginJSON) throws Exception {
		responseEstructure response = new responseEstructure();
		List<loginDTO> sessionsGenerates = new ArrayList<loginDTO>();
		try {
			loginDTO login = (loginDTO) converterManager
					.convert(ConvertFromDataToLoginUps.getInstancia(loginJSON));
			loginDTO loginResp = facturacionServiceEJB.startSession(login);
			if(loginResp!=null){
				response.setMessage("Sesi�n Iniciada");
				response.setStatus(0);
				
				sessionsGenerates.add(loginResp);
				response.setData(sessionsGenerates);
			} else {
				response.setMessage("Usuario/Contrase�a Incorrectos");
				response.setStatus(1);
				response.setData(sessionsGenerates);
			}
			
		} catch (Exception e) {
			//response.setError(e.getError());
			response.setMessage(e.getMessage());
		}
		
		return Response.status(Response.Status.OK)
				.entity(ServicioUtil.getInstancia().createJSONDataResponse(response, converterManager
						.convertFromCollectionToJson(ConvertFromDataToLoginUps.getInstancia(), sessionsGenerates))).build();
	}
	@Override
	public Response getSector(String idLogin) throws Exception {
		responseEstructure response = new responseEstructure();
		List<TblSectorUsuario> userResp = new ArrayList<TblSectorUsuario>();
		List<secComDTO> listSec = new ArrayList<secComDTO>();
		try {
			loginDTO login = (loginDTO) converterManager
					.convert(ConvertFromDataToLoginUps.getInstancia(idLogin));
			userResp = facturacionServiceEJB.buscarSector(login.getIdLogin());
			
			if(userResp!=null){
				response.setMessage("Consulta realizada con �xito");
				response.setStatus(0);
				for(TblSectorUsuario list:userResp){
//					listSec.add(list.getSecComDTO());
//					for(TblSectorUsuario listSecUser:list){
						listSec.add(list.getSecComDTO());
//					}
				}
				response.setData(listSec);
				
				
			} else {
				response.setMessage("No existen Datos");
				response.setStatus(1);
				response.setData(listSec);			}
			
		} catch (Exception e) {
			//response.setError(e.getError());
			response.setMessage(e.getMessage());
		}
		
		return Response.status(Response.Status.OK)
				.entity(ServicioUtil.getInstancia().createJSONDataResponse(response, converterManager
						.convertFromCollectionToJson(ConvertFromDataToSectorUps.getInstancia(), listSec))).build();
	}
	
	@Override
	public Response guardarConsumo(String consumoJson) throws Exception {
		responseEstructure response = new responseEstructure();
		TblConsumo consumoResp = new TblConsumo();
		List<TblConsumo> listCons = new ArrayList<TblConsumo>();
		try {
			TblConsumo consumo = (TblConsumo) converterManagerConsumo
					.convert(ConvertFromDataToConsumoUps.getInstancia(consumoJson));
			
			consumoResp = facturacionServiceEJB.crearConsumo(consumo);
			
			if(consumoResp!=null){
				response.setMessage("Ingreso realizado con �xito");
				response.setStatus(0);
				listCons.add(consumoResp);
				response.setData(listCons);
				
			} else {
				response.setMessage("No existe c�digo de medidor asociado en el sector seleccionado");
				response.setStatus(1);
				response.setData(listCons);			
				}
			
		} catch (Exception e) {
			//response.setError(e.getError());
			response.setMessage(e.getMessage());
		}
		
		return Response.status(Response.Status.OK)
				.entity(ServicioUtil.getInstancia().createJSONDataResponse(response, converterManager
						.convertFromCollectionToJson(ConvertFromDataToConsumoUps.getInstancia(), listCons))).build();
	}
	
	@Override
	public Response removeLoginUps(String loginJSON) throws Exception {
		responseEstructure response = new responseEstructure();
		List<loginDTO> sessionsGenerates = new ArrayList<loginDTO>();
		try {
			loginDTO login = (loginDTO) converterManager
					.convert(ConvertFromDataToLoginUps.getInstancia(loginJSON));
			loginDTO loginResp = facturacionServiceEJB.eliminarUsuario(login);
			sessionsGenerates.add(loginResp);
			
			response.setMessage("Registro eliminado");
			//response.setError(ResultEnum.OK.getValor());
			response.setData(sessionsGenerates);
			
		} catch (Exception e) {
			//response.setError(e.getError());
			response.setMessage(e.getMessage());
		}
		
		return Response.status(Response.Status.OK)
				.entity(ServicioUtil.getInstancia().createJSONDataResponse(response, converterManager
						.convertFromCollectionToJson(ConvertFromDataToLoginUps.getInstancia(), sessionsGenerates))).build();
	}


	

}
