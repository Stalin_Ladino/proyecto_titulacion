package com.ec.ups.servicios.web;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;



@Path(value = "facturacionService")
public interface facturacionService {

	
	@POST
	@Path(value = "/login")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response loginUps(String loginJson) throws Exception;
	
	@POST
	@Path(value = "/getSector")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getSector(String idSector) throws Exception;
	
	@POST
	@Path(value = "/guardarConsumo")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response guardarConsumo(String consumoJson) throws Exception;
	
	
	@POST
	@Path(value = "/removeLoginUps")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response removeLoginUps(String loginJson) throws Exception;
	    

}
