package com.ec.ups.controlador;

import java.io.IOException;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.ec.ups.common.controlador.CommonControlador;
import com.ec.ups.datamanager.loginDatamanager;
import com.ec.ups.datamanager.regionalesDatamanager;
import com.ec.ups.ejb.interfac.impl.regionalDTOInterface;
import com.ec.ups.jpa.entidades.TblRegional;
import com.ec.ups.util.ConstantesUtil;
import com.facturacion.util.web.JsfUtil;

@ManagedBean(name = "regionalesController")
@ViewScoped
public class regionalesController extends CommonControlador {

	private static final long serialVersionUID = 1L;
	private Boolean init = Boolean.TRUE;

	@ManagedProperty(value = "#{regionalesDatamanager}")
	private regionalesDatamanager regionalesDatamanager;

	@EJB
	private regionalDTOInterface regionalDTOInterface;
	
	@ManagedProperty(value = "#{loginDatamanager}")
	private loginDatamanager loginDatamanager;

	@Override
	public void inicializarDatos() {
		if (init) {
			init = Boolean.FALSE;
			
			try {
				this.returnPage();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			
			regionalesDatamanager
					.setVerPantalla(ConstantesUtil.PANTALLA_CONSULTAR);
			regionalesDatamanager.setRegionalList(regionalDTOInterface.findAll());
			regionalesDatamanager.setRegionalFormul(new TblRegional());
			regionalesDatamanager.setRegionalSeleccion(new TblRegional());

		}

	}

	public void returnPage() throws IOException {
		if (null == loginDatamanager.getLoginObj()) {
			JsfUtil.removeManagedBean("loginDataManager");
			JsfUtil.invalidateSession();
			FacesContext fc = FacesContext.getCurrentInstance();
			fc.getExternalContext().redirect(
					"/SistemaFacturacion/page/index2.jsf");
		}
	}
	public void guardar() {
		if(regionalesDatamanager.getAccion().equals(ConstantesUtil.ACCION_NUEVO)){
		regionalDTOInterface.create(regionalesDatamanager.getRegionalSeleccion());
		regionalesDatamanager.setVerPantalla(ConstantesUtil.PANTALLA_CONSULTAR);
		JsfUtil.addMessage(ConstantesUtil.MSJ_INFO,
				"Regional registrado con �xito");
		 } else
		 if
		 (regionalesDatamanager.getAccion().equals(ConstantesUtil.ACCION_MODIFICAR))
		 {
		 regionalDTOInterface.edit(regionalesDatamanager.getRegionalSeleccion());
		 regionalesDatamanager.setVerPantalla(ConstantesUtil.PANTALLA_CONSULTAR);
		 JsfUtil.addMessage(ConstantesUtil.MSJ_INFO,
		 "Registro modificado con �xito");
		 }
		 regionalesDatamanager.setRegionalList(regionalDTOInterface.findAll());
	}

	public void accion(String pantalla) {
		regionalesDatamanager.setVerPantalla(pantalla);
		regionalesDatamanager.setAccion(ConstantesUtil.ACCION_NUEVO);
		regionalesDatamanager.setRegionalSeleccion(new TblRegional());

	}

	public void modificar(TblRegional regionalSelec) {
		regionalesDatamanager.setVerPantalla(ConstantesUtil.PANTALLA_NUEVO);
		regionalesDatamanager.setAccion(ConstantesUtil.ACCION_MODIFICAR);
		regionalesDatamanager.setRegionalSeleccion(regionalSelec);

	}

	public void eliminar(TblRegional regionalSelec) {
		try {
			regionalDTOInterface.remove(regionalSelec);
			JsfUtil.addMessage(ConstantesUtil.MSJ_INFO,"Registro eliminado con �xito");
			regionalesDatamanager.setRegionalList(regionalDTOInterface.findAll());
		} catch (Exception e) {
			JsfUtil.addMessage(ConstantesUtil.MSJ_WARN,"El registro no se pudo eliminar revise dependencias.");
			System.out.println("error->"+e);
		}


	}

	public regionalesDatamanager getRegionalesDatamanager() {
		return regionalesDatamanager;
	}

	public void setRegionalesDatamanager(
			regionalesDatamanager regionalesDatamanager) {
		this.regionalesDatamanager = regionalesDatamanager;
	}

	public loginDatamanager getLoginDatamanager() {
		return loginDatamanager;
	}

	public void setLoginDatamanager(loginDatamanager loginDatamanager) {
		this.loginDatamanager = loginDatamanager;
	}

	

}
