package com.ec.ups.controlador;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.ec.ups.common.controlador.CommonControlador;
import com.ec.ups.datamanager.eventosDatamanager;
import com.ec.ups.datamanager.loginDatamanager;
import com.ec.ups.ejb.interfac.impl.eventosDTOInterface;
import com.ec.ups.jpa.entidades.TblConsumo;
import com.ec.ups.jpa.entidades.TblEvento;
import com.ec.ups.util.ConstantesUtil;
import com.facturacion.util.web.JsfUtil;

@ManagedBean(name = "georeferenciacionController")
@ViewScoped
public class georeferenciacionController extends CommonControlador {

	private static final long serialVersionUID = 1L;
	private Boolean init = Boolean.TRUE;

	@ManagedProperty(value = "#{eventosDatamanager}")
	private eventosDatamanager eventosDatamanager;

	@ManagedProperty(value = "#{loginDatamanager}")
	private loginDatamanager loginDatamanager;

	@EJB
	private eventosDTOInterface eventosDTOInterface;

	@Override
	public void inicializarDatos() {
		if (init) {
			init = Boolean.FALSE;
			eventosDatamanager.setFormulEventos(new TblEvento());
			eventosDatamanager.setListEventos(eventosDTOInterface.findAll());
			eventosDatamanager
			.setVerPantalla(ConstantesUtil.PANTALLA_CONSULTAR);
			eventosDatamanager.setEventSelect(new TblEvento());
		}
	}
	
	public void guardar() {
		eventosDTOInterface.edit(eventosDatamanager.getFormulEventos());
		JsfUtil.addMessage(ConstantesUtil.MSJ_INFO,"Registro modificado con �xito");
		this.accion("C");
	}
	
	public void modificar(TblEvento evento){
		eventosDatamanager.setFormulEventos(evento);
		this.accion("N");
	}
	
	public void accion(String pantalla) {
		eventosDatamanager.setVerPantalla(pantalla);
		eventosDatamanager.setAccion(pantalla);

	}
	
	public void perubas(){
		System.out.println("entros");
	}
	public eventosDatamanager geteventosDatamanager() {
		return eventosDatamanager;
	}

	public void seteventosDatamanager(
			eventosDatamanager eventosDatamanager) {
		this.eventosDatamanager = eventosDatamanager;
	}

	public loginDatamanager getLoginDatamanager() {
		return loginDatamanager;
	}

	public void setLoginDatamanager(loginDatamanager loginDatamanager) {
		this.loginDatamanager = loginDatamanager;
	}

}
