package com.ec.ups.controlador;

import java.io.IOException;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.ec.ups.common.controlador.CommonControlador;
import com.ec.ups.datamanager.cuentaDatamanager;
import com.ec.ups.datamanager.loginDatamanager;
import com.ec.ups.datamanager.usuarioDatamanager;
import com.ec.ups.ejb.interfac.impl.loginDTOInterface;
import com.ec.ups.ejb.interfac.impl.usuarioDTOInterface;
import com.ec.ups.jpa.entidades.loginDTO;
import com.ec.ups.jpa.entidades.usuarioDTO;
import com.ec.ups.util.ConstantesUtil;
import com.facturacion.util.web.JsfUtil;

@ManagedBean(name="cuentaController")
@ViewScoped
public class cuentaController extends CommonControlador{

	
	private static final long serialVersionUID = 1L;
	private Boolean init=Boolean.TRUE;
	
	@ManagedProperty(value = "#{usuarioDatamanager}") 
	private usuarioDatamanager usuarioDatamanager;
	
	@ManagedProperty(value = "#{cuentaDatamanager}") 
	private cuentaDatamanager cuentaDatamanager;
	
	
	@ManagedProperty(value = "#{loginDatamanager}") 
	private loginDatamanager loginDatamanager;
	
	@EJB
	private usuarioDTOInterface usuarioDTOInterface;
	
	@EJB
	private loginDTOInterface loginDTOInterface;
	
	
	@Override
	public void inicializarDatos() {
		if(init){
			init=Boolean.FALSE;
			
			try {
				this.returnPage();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			cuentaDatamanager.setUsuarioFormul(new usuarioDTO());
			cuentaDatamanager.getUsuarioFormul().setLoginDTO(new loginDTO());
			cuentaDatamanager.setUsuarioFormul(
			usuarioDTOInterface.findByIdLogin(loginDatamanager.getLoginObj().getIdLogin()));
		}
	}
	
	public void returnPage() throws IOException {
		if (null == loginDatamanager.getLoginObj()) {
			JsfUtil.removeManagedBean("loginDataManager");
			JsfUtil.invalidateSession();
			FacesContext fc = FacesContext.getCurrentInstance();
			fc.getExternalContext().redirect(
					"/SistemaFacturacion/page/index2.jsf");
		}
	}
	
	public void  guardar(){
		loginDTO login= new loginDTO();
		
		loginDTO loginVal =
				loginDTOInterface.buscarLoginNombre(cuentaDatamanager.getUsuarioFormul().getLoginDTO().getNomUser());
				
				if(null!=loginVal && (loginVal.getIdLogin()!=cuentaDatamanager.getUsuarioFormul().getLoginDTO().getIdLogin())){
					JsfUtil.addMessage(ConstantesUtil.MSJ_ERROR,"El nombre de usuario ya existe");
					return;
				}
		
		if(null!=cuentaDatamanager.getContrasena() &&
				!cuentaDatamanager.getContrasena().equals(ConstantesUtil.CADENA_VACIA)){
			cuentaDatamanager.getUsuarioFormul().getLoginDTO().
					setPassUser(cuentaDatamanager.getContrasena());
		} 
		
		loginDTOInterface.edit(cuentaDatamanager.getUsuarioFormul().getLoginDTO());
//		cuentaDatamanager.setUsuarioFormul(
//				usuarioDTOInterface.findByIdLogin(loginDatamanager.getLoginObj().getIdLogin()));
		JsfUtil.addMessage(ConstantesUtil.MSJ_INFO, "Usuario modificado con �xito");
	}
				
	public usuarioDatamanager getUsuarioDatamanager() {
		return usuarioDatamanager;
	}

	public void setUsuarioDatamanager(usuarioDatamanager usuarioDatamanager) {
		this.usuarioDatamanager = usuarioDatamanager;
	}

	public cuentaDatamanager getCuentaDatamanager() {
		return cuentaDatamanager;
	}

	public void setCuentaDatamanager(cuentaDatamanager cuentaDatamanager) {
		this.cuentaDatamanager = cuentaDatamanager;
	}

	public loginDatamanager getLoginDatamanager() {
		return loginDatamanager;
	}

	public void setLoginDatamanager(loginDatamanager loginDatamanager) {
		this.loginDatamanager = loginDatamanager;
	}
		

}
