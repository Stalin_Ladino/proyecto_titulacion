package com.ec.ups.controlador;

import java.io.IOException;
import java.util.Date;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.ec.ups.common.controlador.CommonControlador;
import com.ec.ups.datamanager.loginDatamanager;
import com.ec.ups.datamanager.multasDatamanager;
import com.ec.ups.ejb.interfac.impl.multaDTOInterface;
import com.ec.ups.ejb.interfac.impl.secComDTOInterface;
import com.ec.ups.ejb.interfac.impl.sectorUsuarioDTOInterface;
import com.ec.ups.ejb.interfac.impl.usuarioDTOInterface;
import com.ec.ups.ejb.interfac.impl.usuarioMultaDTOInterface;
import com.ec.ups.jpa.entidades.TblMulta;
import com.ec.ups.jpa.entidades.TblSectorUsuario;
import com.ec.ups.jpa.entidades.TblUsuarioMulta;
import com.ec.ups.util.ConstantesUtil;
import com.facturacion.util.web.JsfUtil;

@ManagedBean(name = "multasController")
@ViewScoped
public class multasController extends CommonControlador {

	private static final long serialVersionUID = 1L;
	private Boolean init = Boolean.TRUE;

	@ManagedProperty(value = "#{multasDatamanager}")
	private multasDatamanager multasDatamanager;

	@ManagedProperty(value = "#{loginDatamanager}")
	private loginDatamanager loginDatamanager;

	@EJB
	private multaDTOInterface multaDTOInterface;

	@EJB
	private secComDTOInterface secComDTOInterface;

	@EJB
	private usuarioDTOInterface usuarioDTOInterface;

	@EJB
	private usuarioMultaDTOInterface usuarioMultaDTOInterface;
	
	@EJB
	private sectorUsuarioDTOInterface sectorUsuarioDTOInterface;


	@Override
	public void inicializarDatos() {
		if (init) {
			init = Boolean.FALSE;
			
			try {
				this.returnPage();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			multasDatamanager.setVerPantalla(ConstantesUtil.PANTALLA_CONSULTAR);
			multasDatamanager.setmulFormul(new TblMulta());
			multasDatamanager.setUsuarioMulSelec(new TblUsuarioMulta());
			consultarSanciones();

			if (loginDatamanager.getLoginObj().getIdValuenivel() != ConstantesUtil.NIVEL_SUPER_ADMI) {

				multasDatamanager.setSecComList(secComDTOInterface
						.findAllBySector(loginDatamanager.getSectorSeleSec()
								.getIdSec()));
			} else {
				multasDatamanager.setSecComList(secComDTOInterface.findAll());
			}

			if (loginDatamanager.getLoginObj().getIdValuenivel() != ConstantesUtil.NIVEL_SUPER_ADMI) {
				multasDatamanager.setSectorUsuarioList(sectorUsuarioDTOInterface.findBySector(loginDatamanager.getSectorSeleSec().getIdSec()));
			} else {
				multasDatamanager.setSectorUsuarioList(sectorUsuarioDTOInterface.findAll());
			}

			multasDatamanager.setmulFormul(new TblMulta());
			multasDatamanager.setmulSeleccion(new TblMulta());
		}
	}
	
	public void returnPage() throws IOException {
		if (null == loginDatamanager.getLoginObj()) {
			JsfUtil.removeManagedBean("loginDataManager");
			JsfUtil.invalidateSession();
			FacesContext fc = FacesContext.getCurrentInstance();
			fc.getExternalContext().redirect(
					"/SistemaFacturacion/page/index2.jsf");
		}
	}

	public void guardar() {
		if (multasDatamanager.getAccion().equals(ConstantesUtil.ACCION_NUEVO)) {
			multaDTOInterface.create(multasDatamanager.getmulFormul());
			multasDatamanager.setVerPantalla(ConstantesUtil.PANTALLA_CONSULTAR);
			JsfUtil.addMessage(ConstantesUtil.MSJ_INFO,
					"Sanci�n registrado con �xito");
		} else

		if (multasDatamanager.getAccion().equals(
				ConstantesUtil.ACCION_MODIFICAR)) {
			multaDTOInterface.edit(multasDatamanager.getmulFormul());
			multasDatamanager.setVerPantalla(ConstantesUtil.PANTALLA_CONSULTAR);
			JsfUtil.addMessage(ConstantesUtil.MSJ_INFO,
					"Sanci�n modificado con �xito");
		}
		consultarSanciones();
	}

	public void consultarSanciones() {
		if (loginDatamanager.getLoginObj().getIdValuenivel() != ConstantesUtil.NIVEL_SUPER_ADMI) {
			multasDatamanager.setmulList(multaDTOInterface
					.buscarBySector(loginDatamanager.getSectorSeleSec()
							.getIdSec()));
		} else {
			multasDatamanager.setmulList(multaDTOInterface.findByAll());
		}
		
		if (loginDatamanager.getLoginObj().getIdValuenivel() != ConstantesUtil.NIVEL_SUPER_ADMI) {
			multasDatamanager.setSectorUsuarioList(sectorUsuarioDTOInterface.findBySector(loginDatamanager.getSectorSeleSec().getIdSec()));
		} else {
			multasDatamanager.setSectorUsuarioList(sectorUsuarioDTOInterface.findAll());
		}
	}

	public void accion(String pantalla) {
		if(pantalla.equals(ConstantesUtil.ACCION_CONSULTAR)){
			if(null!=multasDatamanager.getSectorUserSeleccionado()){
				multasDatamanager.getSectorUserSeleccionado().clear();
			}
		}
		multasDatamanager.setVerPantalla(pantalla);
		multasDatamanager.setAccion(ConstantesUtil.ACCION_NUEVO);
		multasDatamanager.setmulFormul(new TblMulta());

	}

	public void modificar(TblMulta tblMulta) {
		multasDatamanager.setVerPantalla(ConstantesUtil.PANTALLA_NUEVO);
		multasDatamanager.setAccion(ConstantesUtil.ACCION_MODIFICAR);
		multasDatamanager.setmulFormul(tblMulta);
	}

	public void eliminar(TblMulta tblMulta) {
		tblMulta.setSecComDTO(null);
		multaDTOInterface.remove(tblMulta);
		JsfUtil.addMessage(ConstantesUtil.MSJ_INFO,
				"Sanci�n eliminada con �xito");

		consultarSanciones();
	}

	public void guardarUsuMultas() {
		Date today = new Date();
		TblUsuarioMulta objUserMul = new TblUsuarioMulta();
		for(TblSectorUsuario listSecUser: multasDatamanager.getSectorUserSeleccionado()){
			objUserMul.setIdMulta(multasDatamanager.getmulSeleccion().getIdMulta());
			objUserMul.setIdUsu(listSecUser.getIdUsu());
			objUserMul.setFechaReg(today);
			usuarioMultaDTOInterface.create(objUserMul);
			objUserMul = new TblUsuarioMulta();
			
		}

		JsfUtil.addMessage(ConstantesUtil.MSJ_INFO,
				"Sancionados registrados con �xito");
		multasDatamanager.setVerPantalla(ConstantesUtil.PANTALLA_CONSULTAR);
		multasDatamanager.getSectorUserSeleccionado().clear();
	}

	public void consultarMul(String accion) {
		this.accion(accion);
		if (loginDatamanager.getLoginObj().getIdValuenivel() != ConstantesUtil.NIVEL_SUPER_ADMI) {
			multasDatamanager.setUsuarioMulList(usuarioMultaDTOInterface
					.findBySector(loginDatamanager.getSectorSeleSec()
							.getIdSec()));
		} else {
			multasDatamanager.setUsuarioMulList(usuarioMultaDTOInterface
					.findAll());
		}

	}
	
	public void eliminarMul(TblUsuarioMulta tblUserMul) {
		tblUserMul.setTblMulta(null);
		tblUserMul.setTblUsuario(null);
		usuarioMultaDTOInterface.remove(tblUserMul);
		JsfUtil.addMessage(ConstantesUtil.MSJ_INFO,
				"Sanci�n eliminada con �xito");

		consultarMul("BS");
	}

	public multasDatamanager getMultasDatamanager() {
		return multasDatamanager;
	}

	public void setMultasDatamanager(multasDatamanager multasDatamanager) {
		this.multasDatamanager = multasDatamanager;
	}

	public loginDatamanager getLoginDatamanager() {
		return loginDatamanager;
	}

	public void setLoginDatamanager(loginDatamanager loginDatamanager) {
		this.loginDatamanager = loginDatamanager;
	}

}
