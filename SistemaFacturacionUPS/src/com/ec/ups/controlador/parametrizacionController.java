package com.ec.ups.controlador;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;

import com.ec.ups.common.controlador.CommonControlador;
import com.ec.ups.datamanager.loginDatamanager;
import com.ec.ups.datamanager.menuDatamanager;
import com.ec.ups.datamanager.parametrizacionDatamanager;
import com.ec.ups.ejb.interfac.impl.excedenteDTOInterface;
import com.ec.ups.ejb.interfac.impl.loginDTOInterface;
import com.ec.ups.ejb.interfac.impl.paginaDTOInterface;
import com.ec.ups.ejb.interfac.impl.parametroDTOInterface;
import com.ec.ups.ejb.interfac.impl.secComDTOInterface;
import com.ec.ups.ejb.interfac.impl.serviciosDTOInterface;
import com.ec.ups.jpa.entidades.TblExcedente;
import com.ec.ups.jpa.entidades.TblParametro;
import com.ec.ups.jpa.entidades.secComDTO;
import com.ec.ups.util.ConstantesUtil;
import com.facturacion.util.web.JsfUtil;
import com.facturacion.util.web.MensajesUtil;

@ManagedBean(name = "parametrizacionController")
@ViewScoped
public class parametrizacionController extends CommonControlador {

	private static final long serialVersionUID = 1L;

	private Boolean init = Boolean.TRUE;

	@ManagedProperty(value = "#{menuDatamanager}")
	private menuDatamanager menuDatamanager;

	@ManagedProperty(value = "#{parametrizacionDatamanager}")
	private parametrizacionDatamanager parametrizacionDatamanager;

	@ManagedProperty(value = "#{loginDatamanager}")
	private loginDatamanager loginDatamanager;

	public loginDatamanager getLoginDatamanager() {
		return loginDatamanager;
	}

	public void setLoginDatamanager(loginDatamanager loginDatamanager) {
		this.loginDatamanager = loginDatamanager;
	}

	@EJB
	private loginDTOInterface loginDTOInterface;

	@EJB
	private paginaDTOInterface paginaDTOInterface;

	@EJB
	private parametroDTOInterface parametroDTOInterface;

	@EJB
	private secComDTOInterface secComDTOInterface;

	@EJB
	private serviciosDTOInterface serviciosDTOInterface;

	@EJB
	private excedenteDTOInterface excedenteDTOInterface;

	public void ingresar() throws IOException {
	}

	@Override
	public void inicializarDatos() {
		if (init) {
			init = Boolean.FALSE;
			
			try {
				this.returnPage();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			
			parametrizacionDatamanager
					.setVerPantalla(ConstantesUtil.PANTALLA_CONSULTAR);

			if (loginDatamanager.getLoginObj().getIdValuenivel() != ConstantesUtil.NIVEL_SUPER_ADMI) {

				parametrizacionDatamanager.setSecComList(secComDTOInterface
						.findAllBySector(loginDatamanager.getSectorSeleSec()
								.getIdSec()));
			} else {
				parametrizacionDatamanager.setSecComList(secComDTOInterface
						.findAll());
			}
			parametrizacionDatamanager.setParametroObjs(new TblParametro());
			parametrizacionDatamanager.getParametroObjs().setSecComDTO(
					new secComDTO());

			if (loginDatamanager.getLoginObj().getIdValuenivel() != ConstantesUtil.NIVEL_SUPER_ADMI) {

				parametrizacionDatamanager
						.setListParametros(parametroDTOInterface
								.buscarParamBySect(loginDatamanager
										.getSectorSeleSec().getIdSec()));
			} else {
				parametrizacionDatamanager
						.setListParametros(parametroDTOInterface
								.buscarParamSec());
			}
			parametrizacionDatamanager.setServiciosList(serviciosDTOInterface
					.findAll());
			parametrizacionDatamanager
					.setExcedenteList(new ArrayList<TblExcedente>());
			parametrizacionDatamanager.setExcedenteObj(new TblExcedente());
			parametrizacionDatamanager.setExcedenteFormul(new TblExcedente());

			parametrizacionDatamanager.setExcedenteMap(new HashMap<>());

		}
	}

	public void returnPage() throws IOException {
		if (null == loginDatamanager.getLoginObj()) {
			JsfUtil.removeManagedBean("loginDataManager");
			JsfUtil.invalidateSession();
			FacesContext fc = FacesContext.getCurrentInstance();
			fc.getExternalContext().redirect(
					"/SistemaFacturacion/page/index2.jsf");
		}
	}
	
	public void guardar() {

		if (parametrizacionDatamanager.getAccion().equals(
				ConstantesUtil.ACCION_NUEVO)) {

			parametroDTOInterface.create(parametrizacionDatamanager
					.getParametroObjs());
			parametrizacionDatamanager
					.setVerPantalla(ConstantesUtil.PANTALLA_CONSULTAR);

			for (TblExcedente list : parametrizacionDatamanager
					.getExcedenteList()) {
				list.setIdParam(parametrizacionDatamanager.getParametroObjs()
						.getIdParam());
				excedenteDTOInterface.create(list);
			}

			JsfUtil.addMessage(ConstantesUtil.MSJ_INFO,
					"Par�metro registrado con �xito");

		} else if (parametrizacionDatamanager.getAccion().equals(
				ConstantesUtil.ACCION_MODIFICAR)) {
			parametroDTOInterface.edit(parametrizacionDatamanager
					.getParametroObjs());
			parametrizacionDatamanager
					.setVerPantalla(ConstantesUtil.PANTALLA_CONSULTAR);
			JsfUtil.addMessage(ConstantesUtil.MSJ_INFO,
					"Par�metro modificado con �xito");
		}
		if (loginDatamanager.getLoginObj().getIdValuenivel() != ConstantesUtil.NIVEL_SUPER_ADMI) {

			parametrizacionDatamanager.setListParametros(parametroDTOInterface
					.buscarParamBySect(loginDatamanager.getSectorSeleSec()
							.getIdSec()));
		} else {
			parametrizacionDatamanager.setListParametros(parametroDTOInterface
					.buscarParamSec());
		}
		parametrizacionDatamanager.setExcedenteFormul(new TblExcedente());
	}

	public void modificar(TblParametro parametroSelect) {
		parametrizacionDatamanager
				.setVerPantalla(ConstantesUtil.PANTALLA_NUEVO);
		parametrizacionDatamanager.setAccion(ConstantesUtil.ACCION_MODIFICAR);
		parametrizacionDatamanager.setParametroObjs(parametroSelect);
		parametrizacionDatamanager.getParametroObjs().setSecComDTO(
				parametroSelect.getSecComDTO());
		parametrizacionDatamanager.setExcedenteList(excedenteDTOInterface
				.buscarIdParam(parametroSelect.getIdParam()));
		parametrizacionDatamanager.setExcedenteFormul(new TblExcedente());
	}

	public void eliminar(TblParametro parametroSelect) {
		parametroDTOInterface.remove(parametroSelect);
		JsfUtil.addMessage(ConstantesUtil.MSJ_INFO,
				"Usuario eliminado con �xito");
		if (loginDatamanager.getLoginObj().getIdValuenivel() != ConstantesUtil.NIVEL_SUPER_ADMI) {

			parametrizacionDatamanager.setListParametros(parametroDTOInterface
					.buscarParamBySect(loginDatamanager.getSectorSeleSec()
							.getIdSec()));
		} else {
			parametrizacionDatamanager.setListParametros(parametroDTOInterface
					.buscarParamSec());
		}

	}

	public void accion(String pantalla) {
		parametrizacionDatamanager.setVerPantalla(pantalla);
		parametrizacionDatamanager.setAccion(ConstantesUtil.ACCION_NUEVO);
		if (pantalla.equals(ConstantesUtil.ACCION_NUEVO)) {
			parametrizacionDatamanager
					.setExcedenteList(new ArrayList<TblExcedente>());
		}

	}

	public void onRowEdit(RowEditEvent event) {
		TblExcedente paramTel = (TblExcedente) event.getObject();

		parametrizacionDatamanager.getExcedenteObj();
		try {
			if (parametrizacionDatamanager.getAccion().equals(
					ConstantesUtil.ACCION_MODIFICAR)) {
				excedenteDTOInterface.edit(paramTel);
				parametrizacionDatamanager
						.setExcedenteList(excedenteDTOInterface
								.buscarIdParam(parametrizacionDatamanager
										.getParametroObjs().getIdParam()));
				JsfUtil.addMessage(ConstantesUtil.MSJ_INFO,
						"Registro Modificado con �xito");
			} else {

			}

		} catch (Exception e) {
			throw e;
		}
	}

	public void onRowCancel(RowEditEvent event) {
		JsfUtil.addMessage(ConstantesUtil.MSJ_INFO, MensajesUtil
				.getString("etiqueta.mensaje.almacenamiento.cancelado"));
	}

	public void agregarExcedente2(Integer excObj) {
		System.out.println(excObj);
	}

	public void agregarExcedente(TblExcedente excObj) {

		if (parametrizacionDatamanager.getAccion().equals(
				ConstantesUtil.ACCION_MODIFICAR)) {
			excObj.setIdExc(null);
			excObj.setIdParam(parametrizacionDatamanager.getParametroObjs()
					.getIdParam());
			excedenteDTOInterface.create(excObj);
			excObj = new TblExcedente();
			parametrizacionDatamanager.setExcedenteList(excedenteDTOInterface
					.buscarIdParam(parametrizacionDatamanager
							.getParametroObjs().getIdParam()));

		} else {
			parametrizacionDatamanager.getExcedenteList().add(excObj);
			parametrizacionDatamanager.setExcedenteFormul(new TblExcedente());
			excObj = new TblExcedente();
		}

		parametrizacionDatamanager.setExcedenteFormul(new TblExcedente());
	}

	public void eliminarExcedente(TblExcedente excObj) {
		excedenteDTOInterface.remove(excObj);
		excObj = new TblExcedente();
		parametrizacionDatamanager.setExcedenteList(excedenteDTOInterface
				.buscarIdParam(parametrizacionDatamanager.getParametroObjs()
						.getIdParam()));
	}

	public void modificarRoles(SelectEvent event) {
		parametrizacionDatamanager
				.setExcedenteFormul(parametrizacionDatamanager
						.getExcedenteObj());
	}

	public menuDatamanager getMenuDatamanager() {
		return menuDatamanager;
	}

	public void setMenuDatamanager(menuDatamanager menuDatamanager) {
		this.menuDatamanager = menuDatamanager;
	}

	public parametrizacionDatamanager getParametrizacionDatamanager() {
		return parametrizacionDatamanager;
	}

	public void setParametrizacionDatamanager(
			parametrizacionDatamanager parametrizacionDatamanager) {
		this.parametrizacionDatamanager = parametrizacionDatamanager;
	}
}
