package com.ec.ups.controlador;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import org.primefaces.event.FileUploadEvent;

import com.ec.ups.common.controlador.CommonControlador;
import com.ec.ups.datamanager.eventosDatamanager;
import com.ec.ups.datamanager.loginDatamanager;
import com.ec.ups.ejb.interfac.impl.eventosDTOInterface;
import com.ec.ups.jpa.entidades.TblEvento;
import com.ec.ups.util.ConstantesUtil;
import com.facturacion.util.web.JsfUtil;

@ManagedBean(name = "eventosController")
@ViewScoped
public class eventosController extends CommonControlador {

	private static final long serialVersionUID = 1L;
	private Boolean init = Boolean.TRUE;

	@ManagedProperty(value = "#{eventosDatamanager}")
	private eventosDatamanager eventosDatamanager;

	@ManagedProperty(value = "#{loginDatamanager}")
	private loginDatamanager loginDatamanager;

	@EJB
	private eventosDTOInterface eventosDTOInterface;

	@Override
	public void inicializarDatos() {
		if (init) {
			init = Boolean.FALSE;
			
			try {
				this.returnPage();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			eventosDatamanager.setFormulEventos(new TblEvento());
			eventosDatamanager.setListEventos(eventosDTOInterface.findAll());
			eventosDatamanager
					.setVerPantalla(ConstantesUtil.PANTALLA_CONSULTAR);
			eventosDatamanager.setEventSelect(new TblEvento());

		}
	}

	public void returnPage() throws IOException {
		if (null == loginDatamanager.getLoginObj()) {
			JsfUtil.removeManagedBean("loginDataManager");
			JsfUtil.invalidateSession();
			FacesContext fc = FacesContext.getCurrentInstance();
			fc.getExternalContext().redirect(
					"/SistemaFacturacion/page/index2.jsf");
		}
	}

	public void guardar() {
		String cad = "\n"
				+ eventosDatamanager.getFormulEventos().getDescripcionEvent();
		eventosDatamanager.getFormulEventos().setDescripcionEvent(cad);
		eventosDTOInterface.edit(eventosDatamanager.getFormulEventos());
		JsfUtil.addMessage(ConstantesUtil.MSJ_INFO,
				"Registro modificado con �xito");
		this.accion("C");
	}

	public void modificar(TblEvento evento) {
		eventosDatamanager.setFormulEventos(evento);
		this.accion("N");
	}

	public void accion(String pantalla) {
		eventosDatamanager.setVerPantalla(pantalla);
		eventosDatamanager.setAccion(pantalla);

	}

	public void perubas() {
		System.out.println("entros");
	}

	public void upload(FileUploadEvent event) {
		try {

			if (null == eventosDatamanager.getImagenes()
					|| eventosDatamanager.getImagenes().equals(
							ConstantesUtil.CADENA_VACIA)) {
				JsfUtil.addMessage(ConstantesUtil.MSJ_ERROR,
						"Seleccionar imagen a reemplazar es requerido.");
				return;

			}
			copyFile(event.getFile().getFileName(), event.getFile()
					.getInputstream());
			FacesMessage message = new FacesMessage(
					"El archivo se ha subido con �xito!");

			FacesContext.getCurrentInstance().addMessage(null, message);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void copyFile(String fileName, InputStream in) {
		try {
			String pat = "/resources/facturacion/images/";
			ServletContext servletContext = (ServletContext) FacesContext
					.getCurrentInstance().getExternalContext().getContext();
			String rutaImagenes = servletContext.getRealPath(pat);

			OutputStream out = new FileOutputStream(new File(rutaImagenes
					+ "/imagen" + eventosDatamanager.getImagenes()));
			int read = 0;
			byte[] bytes = new byte[1024];
			while ((read = in.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
			in.close();
			out.flush();
			out.close();
			// DateFormat dateFormat = new
			// SimpleDateFormat("yyyy-MM-dd HH_mm_ss");
			// Date date = new Date();
			// String ruta1 = destination + fileName;
			// String ruta2 = destination + dateFormat.format(date) + "-"
			// + fileName;
			// System.out.println("Archivo: " + ruta1 + " Renombrado a: " +
			// ruta2);
			// File archivo = new File(ruta1);
			// archivo.renameTo(new File(ruta2));
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

	public void eventoDrop() {
		System.out.println("ff" + eventosDatamanager.getImagenes());
	}

	public eventosDatamanager geteventosDatamanager() {
		return eventosDatamanager;
	}

	public void seteventosDatamanager(eventosDatamanager eventosDatamanager) {
		this.eventosDatamanager = eventosDatamanager;
	}

	public loginDatamanager getLoginDatamanager() {
		return loginDatamanager;
	}

	public void setLoginDatamanager(loginDatamanager loginDatamanager) {
		this.loginDatamanager = loginDatamanager;
	}

}
