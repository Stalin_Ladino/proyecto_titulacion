package com.ec.ups.controlador;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import com.ec.ups.common.controlador.CommonControlador;
import com.ec.ups.datamanager.facturacionDatamanager;
import com.ec.ups.datamanager.loginDatamanager;
import com.ec.ups.ejb.interfac.impl.consumoDTOInterface;
import com.ec.ups.ejb.interfac.impl.excedenteDTOInterface;
import com.ec.ups.ejb.interfac.impl.facturaDTOInterface;
import com.ec.ups.ejb.interfac.impl.medidoresDTOInterface;
import com.ec.ups.ejb.interfac.impl.multaDTOInterface;
import com.ec.ups.ejb.interfac.impl.parametroDTOInterface;
import com.ec.ups.ejb.interfac.impl.secComDTOInterface;
import com.ec.ups.ejb.interfac.impl.serviciosDTOInterface;
import com.ec.ups.ejb.interfac.impl.usuarioDTOInterface;
import com.ec.ups.ejb.interfac.impl.usuarioMultaDTOInterface;
import com.ec.ups.jpa.entidades.ResultadoFactura;
import com.ec.ups.jpa.entidades.TblExcedente;
import com.ec.ups.jpa.entidades.TblFactura;
import com.ec.ups.jpa.entidades.TblParametro;
import com.ec.ups.jpa.entidades.TblServicio;
import com.ec.ups.jpa.entidades.TblUsuarioMulta;
import com.ec.ups.jpa.entidades.secComDTO;
import com.ec.ups.jpa.entidades.usuarioDTO;
import com.ec.ups.util.ConstantesUtil;
import com.facturacion.util.web.JsfUtil;
import com.facturacion.util.web.ReportServlet;

@ManagedBean(name = "facturacionController")
@ViewScoped
public class facturacionController extends CommonControlador {

	private static final long serialVersionUID = 1L;
	private Boolean init = Boolean.TRUE;

	@ManagedProperty(value = "#{facturacionDatamanager}")
	private facturacionDatamanager facturacionDatamanager;

	@ManagedProperty(value = "#{loginDatamanager}")
	private loginDatamanager loginDatamanager;

	@EJB
	private multaDTOInterface multaDTOInterface;

	@EJB
	private secComDTOInterface secComDTOInterface;

	@EJB
	private usuarioDTOInterface usuarioDTOInterface;

	@EJB
	private medidoresDTOInterface medidoresDTOInterface;

	@EJB
	private serviciosDTOInterface serviciosDTOInterface;

	@EJB
	private facturaDTOInterface facturaDTOInterface;

	@EJB
	private parametroDTOInterface parametroDTOInterface;

	@EJB
	private excedenteDTOInterface excedenteDTOInterface;

	@EJB
	private usuarioMultaDTOInterface usuarioMultaDTOInterface;
	
	@EJB
	private consumoDTOInterface consumoDTOInterface;
	

	@Override
	public void inicializarDatos() {
		if (init) {
			init = Boolean.FALSE;
			try {
				this.returnPage();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			facturacionDatamanager
					.setVerPantalla(ConstantesUtil.ACCION_CONSULTAR);
			facturacionDatamanager.setUsuarioSelect(new usuarioDTO());
			facturacionDatamanager.setServicioSelect(new TblServicio());
			facturacionDatamanager.setSecComSelect(new secComDTO());

			if (loginDatamanager.getLoginObj().getIdValuenivel() != ConstantesUtil.NIVEL_SUPER_ADMI) {

				facturacionDatamanager.setSecComList(secComDTOInterface
						.findAllBySector(loginDatamanager.getSectorSeleSec()
								.getIdSec()));
			} else {
				facturacionDatamanager.setSecComList(secComDTOInterface
						.findAll());
			}

			facturacionDatamanager.setServiciosList(serviciosDTOInterface
					.findAll());

			for (TblServicio list : facturacionDatamanager.getServiciosList()) {
				facturacionDatamanager.getMapServicios().put(list.getIdServ(),
						list);
			}
			if(null!=facturacionDatamanager.getResultadoList() && !facturacionDatamanager.getResultadoList().isEmpty()){
				facturacionDatamanager.getResultadoList().clear();
			}
			
			this.initVariables();
		}
	}
	
	public void returnPage() throws IOException {
		if (null == loginDatamanager.getLoginObj()) {
			JsfUtil.removeManagedBean("loginDataManager");
			JsfUtil.invalidateSession();
			FacesContext fc = FacesContext.getCurrentInstance();
			fc.getExternalContext().redirect(
					"/SistemaFacturacion/page/index2.jsf");
		}
	}

	public void consultar() {

		if (null == facturacionDatamanager.getCedula()
				|| facturacionDatamanager.getCedula().equals(
						ConstantesUtil.CADENA_VACIA)) {
			if (loginDatamanager.getLoginObj().getIdValuenivel() != ConstantesUtil.NIVEL_SUPER_ADMI) {

				facturacionDatamanager.setResultadoList(facturaDTOInterface
						.cargarFactura(facturacionDatamanager.getCedula(),
								loginDatamanager.getSectorSeleSec().getIdSec(),
								facturacionDatamanager.getFechaIni(),
								facturacionDatamanager.getFechaFin(),
								facturacionDatamanager.getServicioSelect()
										.getIdServ()));
			} else {
				facturacionDatamanager.setResultadoList(facturaDTOInterface
						.cargarFactura(facturacionDatamanager.getCedula(),
								loginDatamanager.getSectorSeleSec().getIdSec(),
								facturacionDatamanager.getFechaIni(),
								facturacionDatamanager.getFechaFin(),
								facturacionDatamanager.getServicioSelect()
										.getIdServ()));
			}

		} else {
			usuarioDTO usuario = new usuarioDTO();

			usuario = usuarioDTOInterface.findByCedula(facturacionDatamanager
					.getCedula());
			if (null == usuario) {
				JsfUtil.addMessage(ConstantesUtil.MSJ_INFO,"N�mero de c�dula no registrado.");
				facturacionDatamanager.setResultadoList(new ArrayList<>());
				return;
			} else {
				facturacionDatamanager.getUsuarioList().clear();
				facturacionDatamanager.getUsuarioList().add(usuario);

				// Carga de resultado
				facturacionDatamanager.setResultadoList(facturaDTOInterface
						.cargarFactura(facturacionDatamanager.getCedula(),
								facturacionDatamanager.getSecComSelect()
										.getIdSec(), facturacionDatamanager
										.getFechaIni(), facturacionDatamanager
										.getFechaFin(), facturacionDatamanager
										.getServicioSelect().getIdServ()));
			}
		}
	}

	public void consultarFactSect() {

		facturacionDatamanager.setUsuarioSector(usuarioDTOInterface
				.buscarByCedulaSector(facturacionDatamanager.getCedula(),
						facturacionDatamanager.getSecComSelect().getIdSec())
				.get(0));

		facturacionDatamanager.setMedidorList(medidoresDTOInterface
				.buscarPorIdSector(facturacionDatamanager.getUsuarioSector()
						.getIdUsu(), facturacionDatamanager.getSecComSelect()
						.getIdSec()));

	}

	public void consultarMedSect() {

		medidoresDTOInterface.buscarPorIdSector(facturacionDatamanager
				.getUsuarioSector().getIdUsu(), facturacionDatamanager
				.getSecComSelect().getIdSec());

	}

	public void ver(ResultadoFactura resFact) throws Exception {
		facturacionDatamanager.setVerPantalla(ConstantesUtil.ACCION_MODIFICAR);
		facturacionDatamanager.setResultadoSelect(resFact);
		facturacionDatamanager.setConsumoTotal(resFact.getTot_consum());

		facturacionDatamanager.setMultasList(multaDTOInterface
				.buscarBySector(facturacionDatamanager.getResultadoSelect()
						.getId_sec()));

		this.fechas(resFact.getFecha_reg());
		facturacionDatamanager.setUsuarioMultasList(usuarioMultaDTOInterface
				.findBySectorUserFecha(facturacionDatamanager
						.getResultadoSelect().getId_sec(), resFact.getId_usu(),
						facturacionDatamanager.getFechIniMulta(),
						facturacionDatamanager.getFechFinMulta()));

		if (!calcularValor()) {
			facturacionDatamanager
					.setVerPantalla(ConstantesUtil.ACCION_CONSULTAR);
			return;
		}

	}

	public Boolean calcularValor() {

		List<TblParametro> parametroList = parametroDTOInterface
				.buscarParamBySectServ(facturacionDatamanager
						.getResultadoSelect().getId_sec(),
						facturacionDatamanager.getResultadoSelect()
								.getId_serv());
		Double resp = 0.0;
		Double totalAPagarConsumo=0.0;
		Double totalMultasConsumo = 0.0;
		
		if (parametroList.size() > 1) {
			JsfUtil.addMessage(ConstantesUtil.MSJ_INFO,
					"Revise tabla de par�metros existen dos"
							+ " valores para el servicio seleccionado.");
			return Boolean.FALSE;
		} else {
			if (parametroList.isEmpty()) {
				JsfUtil.addMessage(
						ConstantesUtil.MSJ_INFO,
						"Revise tabla de par�metros no existen valores "
								+ "para realizar el calculo de su valor a cancelar.");
				return Boolean.FALSE;
			}

			TblParametro parametro = parametroList.get(0);
			if (facturacionDatamanager.getResultadoSelect().getTot_consum() >= parametro
					.getRegMin()
					&& facturacionDatamanager.getResultadoSelect()
							.getTot_consum() <= parametro.getRegMax()) {
				resp=parametro.getValMcub();
				totalAPagarConsumo=parametro.getValMcub();
				
				facturacionDatamanager.setValorPagarMedidor(this.Redondear(resp));
				facturacionDatamanager.setValorPagarExcedente(0.0);
				
			} else {
				resp = formulaFacturacionExc(facturacionDatamanager
						.getResultadoSelect().getTot_consum(),
						parametro.getIdParam());
				facturacionDatamanager.setValorPagarMedidor(this.Redondear(parametro.getValMcub()));
				totalAPagarConsumo=resp+parametro.getValMcub();
				
			}

			facturacionDatamanager.setTarifaMedidor(parametro.getValMcub().toString());
			facturacionDatamanager.setValorMul(calculoMultas());
			
			facturacionDatamanager.setValorPagar(this.Redondear(totalAPagarConsumo));
			totalMultasConsumo = totalAPagarConsumo + calculoMultas();
			facturacionDatamanager.setValorTotal(this.Redondear(totalMultasConsumo));
			return Boolean.TRUE;
		}
	}

	public Double formulaFacturacion(Long totConsumo, Double costoMC,Double valMax) {
		Double calc = (totConsumo * costoMC) / valMax;
		return calc;
	}

	public Double formulaFacturacionExc(Long totConsumo, Integer idPara) {
		Double valExce = 0.0;
		Double regMax = 0.0;
		Double regMin = 0.0;
		Double calculoExcedente=0.0;
		List<TblExcedente> excList = excedenteDTOInterface
				.buscarIdParam(idPara);
		Double totalExcedente=0.0;

		if (null != excList && !excList.isEmpty()) {
			for (TblExcedente list : excList) {
				if (totConsumo > list.getRegExcMin() && totConsumo < list.getRegExcMax()) {
					valExce = list.getValExcMcub();
					regMax = list.getRegExcMax();
					regMin = list.getRegExcMin();
					calculoExcedente= totConsumo-regMin;
					totalExcedente=totalExcedente+(calculoExcedente*valExce);
					break;
				} else {
					valExce = list.getValExcMcub();
					regMax = list.getRegExcMax();
					regMin = list.getRegExcMin();
					calculoExcedente= totConsumo-regMin;
					totalExcedente=totalExcedente+(calculoExcedente*valExce);
				
				}
			}
		} else {
		//Cuando no existe excedente
			valExce = 0.0;
			regMax = 1.0;
			regMin = (double) totConsumo;
		}
		facturacionDatamanager.getResultadoSelect().setTot_consum(regMin.longValue());
		
		facturacionDatamanager.setTarifaExcedente(valExce.toString());
		
		facturacionDatamanager.setCalculoExcedente(calculoExcedente);
		Double calc = totalExcedente;
		facturacionDatamanager.setValorPagarExcedente(this.Redondear(calc));
		return calc;
	}
	
	

	public Double calculoMultas() {
		Double resp = 0.0;
		if (null != facturacionDatamanager.getUsuarioMultasList()
				&& !facturacionDatamanager.getUsuarioMultasList().isEmpty()) {
			for (TblUsuarioMulta list : facturacionDatamanager
					.getUsuarioMultasList()) {
				resp = resp + list.getTblMulta().getValMulta();
			}
		}

		return resp;
	}
	
	public Double Redondear(Double numero)
	{
	       return Math.rint(numero*100)/100;
	}

	public void fechas(Date fechMulta) {
		Date ultimoDiaMes = this.ponerDiasFechaFinMes(fechMulta);
		Date primerDiaMes = this.ponerDiasFechaInicioMes(fechMulta);

		SimpleDateFormat formatoDeFecha = new SimpleDateFormat("yyyy/MM/dd");
		facturacionDatamanager.setFechIniMulta(formatoDeFecha
				.format(primerDiaMes));
		facturacionDatamanager.setFechFinMulta(formatoDeFecha
				.format(ultimoDiaMes));
	}

	public Date ponerDiasFechaFinMes(Date fecha) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fecha); // Configuramos la fecha que se recibe
		calendar.set(calendar.get(Calendar.YEAR),
				(calendar.get(Calendar.MONTH)),
				calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		return calendar.getTime(); // Devuelve el objeto Date con los nuevos
									// d�as a�adidos

	}

	public Date ponerDiasFechaInicioMes(Date fecha) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fecha); // Configuramos la fecha que se recibe
		calendar.set(calendar.get(Calendar.YEAR),
				(calendar.get(Calendar.MONTH)),
				calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
		return calendar.getTime(); // Devuelve el objeto Date con los nuevos
									// d�as a�adidos

	}

	public void cancelar() {
		facturacionDatamanager.setVerPantalla(ConstantesUtil.ACCION_CONSULTAR);
		facturacionDatamanager.getResultadoList().clear();
		consultar();
		this.initVariables();
	}
	
	public void initVariables(){
		facturacionDatamanager.setTarifaMedidor(null);
		facturacionDatamanager.setValorPagarMedidor(null);
		facturacionDatamanager.setCalculoExcedente(null);
		facturacionDatamanager.setTarifaExcedente(null);
		facturacionDatamanager.setValorPagarExcedente(null);
		facturacionDatamanager.setValorPagar(null);
		facturacionDatamanager.setValorMul(null);
		facturacionDatamanager.setValorPagar(null);
		facturacionDatamanager.setValorTotal(null);
	}
	
	public void validarPago() throws Exception{
		if(!facturacionDatamanager.getResultadoSelect().getEstado_facturado()){
			this.guardarFactura();
			this.modificarEstado(facturacionDatamanager.getResultadoSelect().getId_consum());
			facturacionDatamanager.getResultadoSelect().setEstado_facturado(Boolean.TRUE);
			JsfUtil.addMessage(ConstantesUtil.MSJ_INFO,"Factura Registrada con �xito.");
		} else {
			JsfUtil.addMessage(ConstantesUtil.MSJ_INFO,"Esta factura ya ha sido registrada.");
		}
	}
	
	
	public void generarFactura() throws Exception{
	
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		Map<String, Object> parametros = new HashMap<String, Object>();  
		parametros.put(ReportServlet.TIPO_REPORTE, ReportServlet.REPORTE_PDF);
		parametros.put(ReportServlet.TIPO_SELECCIONADO, ReportServlet.TIPO_FACTURACION);
		parametros.put(ReportServlet.CONEXION, "no");
		parametros.put("cedula", facturacionDatamanager.getResultadoSelect().getCed_usu());
		parametros.put("direccion", facturacionDatamanager.getResultadoSelect().getDir_usu());
		parametros.put("usuario", facturacionDatamanager.getResultadoSelect().getNom_usu());
		parametros.put("codMedidor", facturacionDatamanager.getResultadoSelect().getId_cod_med());
		parametros.put("servicio", facturacionDatamanager.getResultadoSelect().getNom_serv());
		parametros.put("fechDesde", formato.format(this.ponerDiasFechaInicioMes(facturacionDatamanager.getResultadoSelect().getFecha_reg())).toString());
		parametros.put("fechHasta", formato.format(this.ponerDiasFechaFinMes(facturacionDatamanager.getResultadoSelect().getFecha_reg())).toString());
		parametros.put("diasFacturados", "30");
		parametros.put("consumoMed", facturacionDatamanager.getResultadoSelect().getTot_consum().toString());
		parametros.put("tarifaMed", facturacionDatamanager.getTarifaMedidor().toString());
		parametros.put("totalMed", facturacionDatamanager.getValorPagarMedidor().toString());
		parametros.put("consumoExc",null!=facturacionDatamanager.getCalculoExcedente()?facturacionDatamanager.getCalculoExcedente().toString():"0");
		parametros.put("tarifaExc", null!=facturacionDatamanager.getTarifaExcedente()?facturacionDatamanager.getTarifaExcedente().toString():"0");
		parametros.put("totalExc",  null!=facturacionDatamanager.getValorPagarExcedente()?facturacionDatamanager.getValorPagarExcedente().toString():"0");
		parametros.put("valConsumo",  facturacionDatamanager.getValorPagar().toString());
		parametros.put("totalSanciones",  facturacionDatamanager.getValorMul().toString());
		parametros.put("totalDetalle", facturacionDatamanager.getValorPagar().toString());
		parametros.put("totalTodo", facturacionDatamanager.getValorTotal().toString());
		
		
		HttpServletRequest request = JsfUtil.getRequest();
		request.getSession().setAttribute(ReportServlet.OBJETO_REPORTE, parametros);
	}
	
	public void guardarFactura() throws Exception{
		facturaDTOInterface.create(crearObjFactura());
	}
	
	public void modificarEstado(Integer idConsumo) throws Exception{
		consumoDTOInterface.modificarEstado(idConsumo);
	}
	
	public TblFactura crearObjFactura() throws Exception{
		TblFactura objFactura=new TblFactura();
		objFactura.setCedulaUsu(facturacionDatamanager.getResultadoSelect().getCed_usu());
		objFactura.setConsumoM3(facturacionDatamanager.getConsumoTotal());
		objFactura.setFechFact(facturacionDatamanager.getResultadoSelect().getFecha_reg());
		objFactura.setIdFact(null);
		objFactura.setIdMedFact(facturacionDatamanager.getResultadoSelect().getId_med());
		objFactura.setIdSectFact(facturacionDatamanager.getResultadoSelect().getId_sec());
		objFactura.setIdUsuFact(facturacionDatamanager.getResultadoSelect().getId_usu());
		objFactura.setTotalPagar(facturacionDatamanager.getValorTotal());
		objFactura.setTotConsumo(facturacionDatamanager.getValorPagar());
		objFactura.setTotSanciones(facturacionDatamanager.getValorMul());
		return objFactura;
	}

	public facturacionDatamanager getfacturacionDatamanager() {
		return facturacionDatamanager;
	}

	public void setfacturacionDatamanager(
			facturacionDatamanager facturacionDatamanager) {
		this.facturacionDatamanager = facturacionDatamanager;
	}

	public loginDatamanager getLoginDatamanager() {
		return loginDatamanager;
	}

	public void setLoginDatamanager(loginDatamanager loginDatamanager) {
		this.loginDatamanager = loginDatamanager;
	}

}
