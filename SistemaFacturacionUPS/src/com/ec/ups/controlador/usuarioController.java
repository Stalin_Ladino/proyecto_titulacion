package com.ec.ups.controlador;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;

import com.ec.ups.common.controlador.CommonControlador;
import com.ec.ups.datamanager.loginDatamanager;
import com.ec.ups.datamanager.usuarioDatamanager;
import com.ec.ups.ejb.interfac.impl.loginDTOInterface;
import com.ec.ups.ejb.interfac.impl.rolDTOInterface;
import com.ec.ups.ejb.interfac.impl.secComDTOInterface;
import com.ec.ups.ejb.interfac.impl.sectorUsuarioDTOInterface;
import com.ec.ups.ejb.interfac.impl.usuarioDTOInterface;
import com.ec.ups.jpa.entidades.TblSectorUsuario;
import com.ec.ups.jpa.entidades.loginDTO;
import com.ec.ups.jpa.entidades.rolDTO;
import com.ec.ups.jpa.entidades.secComDTO;
import com.ec.ups.jpa.entidades.usuarioDTO;
import com.ec.ups.util.ConstantesUtil;
import com.ec.ups.util.Validador;
import com.facturacion.util.web.JsfUtil;

@ManagedBean(name = "usuarioController")
@ViewScoped
public class usuarioController extends CommonControlador {

	private static final long serialVersionUID = 1L;
	private Boolean init = Boolean.TRUE;

	@ManagedProperty(value = "#{usuarioDatamanager}")
	private usuarioDatamanager usuarioDatamanager;

	@ManagedProperty(value = "#{loginDatamanager}")
	private loginDatamanager loginDatamanager;

	@EJB
	private usuarioDTOInterface usuarioDTOInterface;

	@EJB
	private rolDTOInterface rolDTOInterface;

	@EJB
	private secComDTOInterface secComDTOInterface;

	@EJB
	private loginDTOInterface loginDTOInterface;

	@EJB
	private sectorUsuarioDTOInterface sectorUsuarioDTOInterface;

	@Override
	public void inicializarDatos() {
		if (init) {
			init = Boolean.FALSE;
			
			try {
				this.returnPage();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			usuarioDatamanager
					.setTblSectorUsuarioFormul(new TblSectorUsuario());

			usuarioDatamanager.setExistUser(Boolean.FALSE);
			usuarioDatamanager
					.setVerPantalla(ConstantesUtil.PANTALLA_CONSULTAR);

			if (loginDatamanager.getLoginObj().getIdValuenivel() != ConstantesUtil.NIVEL_SUPER_ADMI) {
				usuarioDatamanager
						.setSectorUsuarioList(sectorUsuarioDTOInterface
								.findBySector(loginDatamanager
										.getSectorSeleSec().getIdSec()));
			} else {
				usuarioDatamanager
						.setSectorUsuarioList(sectorUsuarioDTOInterface
								.findAll());
			}

			usuarioDatamanager.setUsuarioFormul(new usuarioDTO());
			
			
			if (loginDatamanager.getLoginObj().getIdValuenivel() != ConstantesUtil.NIVEL_SUPER_ADMI) {
				usuarioDatamanager.setRolList(rolDTOInterface.findByRolAdministrator());
			} else {
				usuarioDatamanager.setRolList(rolDTOInterface.findAll());
			}
			

			if (loginDatamanager.getLoginObj().getIdValuenivel() != ConstantesUtil.NIVEL_SUPER_ADMI) {

				usuarioDatamanager.setSecComList(secComDTOInterface
						.findAllBySector(loginDatamanager.getSectorSeleSec()
								.getIdSec()));
			} else {
				usuarioDatamanager.setSecComList(secComDTOInterface.findAll());
			}

			usuarioDatamanager.setUsuarioFormul(new usuarioDTO());
			usuarioDatamanager.getUsuarioFormul().setLoginDTO(new loginDTO());
			usuarioDatamanager
					.setSectorUserSeleccionado(new TblSectorUsuario());
			usuarioDatamanager.getSectorUserSeleccionado().setLoginDTO(
					new loginDTO());
			usuarioDatamanager.getSectorUserSeleccionado().setSecComDTO(
					new secComDTO());
			usuarioDatamanager.getSectorUserSeleccionado().setTblRol(
					new rolDTO());
			usuarioDatamanager.getSectorUserSeleccionado().setTblUsuario(
					new usuarioDTO());
			usuarioDatamanager.setValidarCedula(Boolean.FALSE);
		}
	}
	
	public void returnPage() throws IOException {
		if (null == loginDatamanager.getLoginObj()) {
			JsfUtil.removeManagedBean("loginDataManager");
			JsfUtil.invalidateSession();
			FacesContext fc = FacesContext.getCurrentInstance();
			fc.getExternalContext().redirect(
					"/SistemaFacturacion/page/index2.jsf");
		}
	}


	public void validarCampos() {
		
		loginDTO login =
		loginDTOInterface.buscarLoginNombre(usuarioDatamanager.getUsuarioFormul().getLoginDTO().getNomUser());
		
		if(null!=login && !usuarioDatamanager.getAccion().equals(ConstantesUtil.ACCION_MODIFICAR)
				&& null==usuarioDatamanager.getUsuarioFormul().getLoginDTO().getIdLogin()){
			JsfUtil.addMessage(ConstantesUtil.MSJ_ERROR,"El nombre de usuario ya existe");
			return;
		}
		
		if (usuarioDatamanager.getUsuarioFormul().getCedUsu().length() <= 9) {
			usuarioDatamanager.setValidarCedula(Boolean.TRUE);
			return;
		} else {
			if (!Validador.validarCedula(usuarioDatamanager.getUsuarioFormul()
					.getCedUsu())) {
				usuarioDatamanager.setValidarCedula(Boolean.TRUE);
				return;
			} else {
				this.guardar();
				usuarioDatamanager.setValidarCedula(Boolean.FALSE);
			}
		}

	}

	public void guardar() {
		usuarioDatamanager.setValidarCedula(Boolean.FALSE);
		if (usuarioDatamanager.getAccion().equals(ConstantesUtil.ACCION_NUEVO)) {

			if (!usuarioDatamanager.getExistUser()) {
				if (validarContrasena()) {
					return;
				}
			}

			if (null != usuarioDatamanager.getContrasena()) {
				usuarioDatamanager.getUsuarioFormul().getLoginDTO()
						.setPassUser(usuarioDatamanager.getContrasena());
			}

			if (!usuarioDatamanager.getExistUser()) {

				loginDTOInterface.create(usuarioDatamanager.getUsuarioFormul()
						.getLoginDTO());

				usuarioDatamanager.getUsuarioFormul().setIdLogin(
						usuarioDatamanager.getUsuarioFormul().getLoginDTO()
								.getIdLogin());

				usuarioDatamanager.getTblSectorUsuarioFormul().setIdLogin(
						usuarioDatamanager.getUsuarioFormul().getLoginDTO()
								.getIdLogin());

			} else {

				List<usuarioDTO> list = usuarioDTOInterface
						.buscarByCedula(usuarioDatamanager.getUsuarioFormul()
								.getCedUsu());

				List<TblSectorUsuario> listSecUse = sectorUsuarioDTOInterface
						.buscarPorCedula(usuarioDatamanager.getUsuarioFormul()
								.getCedUsu());
				for (TblSectorUsuario secUser : listSecUse) {
					if (secUser.getIdSec().equals(
							usuarioDatamanager.getTblSectorUsuarioFormul()
									.getIdSec())) {
						JsfUtil.addMessage(ConstantesUtil.MSJ_INFO,
								"El usuario ya existe en el sector seleccionado");
						return;
					}
				}
				usuarioDatamanager.getUsuarioFormul().setIdLogin(
						usuarioDatamanager.getExistUserObj().getIdLogin());

				usuarioDatamanager.getTblSectorUsuarioFormul().setIdLogin(
						usuarioDatamanager.getUsuarioFormul().getLoginDTO()
								.getIdLogin());
			}

			// Verifico si el usuario ya existe y no lo creo nuevamente
			if (!usuarioDatamanager.getExistUser()) {
				usuarioDTOInterface.create(usuarioDatamanager
						.getUsuarioFormul());
			}
			usuarioDatamanager.getTblSectorUsuarioFormul().setIdUsu(
					usuarioDatamanager.getUsuarioFormul().getIdUsu());

			sectorUsuarioDTOInterface.create(usuarioDatamanager
					.getTblSectorUsuarioFormul());

			usuarioDatamanager
					.setVerPantalla(ConstantesUtil.PANTALLA_CONSULTAR);
			JsfUtil.addMessage(ConstantesUtil.MSJ_INFO,
					"Usuario registrado con �xito");

		} else if (usuarioDatamanager.getAccion().equals(
				ConstantesUtil.ACCION_MODIFICAR)) {

			if (null != usuarioDatamanager.getContrasena()
					&& !usuarioDatamanager.getContrasena().equals(
							ConstantesUtil.CADENA_VACIA)) {
				usuarioDatamanager.getUsuarioFormul().getLoginDTO()
						.setPassUser(usuarioDatamanager.getContrasena());
			}

			usuarioDTOInterface.edit(usuarioDatamanager.getUsuarioFormul());
			sectorUsuarioDTOInterface.edit(usuarioDatamanager
					.getTblSectorUsuarioFormul());

			usuarioDatamanager
					.setVerPantalla(ConstantesUtil.PANTALLA_CONSULTAR);
			JsfUtil.addMessage(ConstantesUtil.MSJ_INFO,
					"Usuario modificado con �xito");
		}

		if (loginDatamanager.getLoginObj().getIdValuenivel() != ConstantesUtil.NIVEL_SUPER_ADMI) {
			usuarioDatamanager.setSectorUsuarioList(sectorUsuarioDTOInterface
					.findBySector(loginDatamanager.getSectorSeleSec()
							.getIdSec()));
		} else {
			usuarioDatamanager.setSectorUsuarioList(sectorUsuarioDTOInterface
					.findAll());
		}
	}

	public void accion(String pantalla) {
		usuarioDatamanager.setExistUser(Boolean.FALSE);
		usuarioDatamanager.setValidarCedula(Boolean.FALSE);
		usuarioDatamanager.setVerPantalla(pantalla);
		usuarioDatamanager.setAccion(ConstantesUtil.ACCION_NUEVO);
		usuarioDatamanager.setUsuarioFormul(new usuarioDTO());
		usuarioDatamanager.getUsuarioFormul().setLoginDTO(new loginDTO());
		usuarioDatamanager.setTblSectorUsuarioFormul(new TblSectorUsuario());

		if (pantalla.equals(ConstantesUtil.ACCION_CONSULTAR)) {
			if (loginDatamanager.getLoginObj().getIdValuenivel() != ConstantesUtil.NIVEL_SUPER_ADMI) {
				usuarioDatamanager
						.setSectorUsuarioList(sectorUsuarioDTOInterface
								.findBySector(loginDatamanager
										.getSectorSeleSec().getIdSec()));
			} else {
				usuarioDatamanager
						.setSectorUsuarioList(sectorUsuarioDTOInterface
								.findAll());
			}
		}
	}

	public void modificar(TblSectorUsuario usuarioSelec) {
		usuarioDatamanager.setExistUser(Boolean.FALSE);
		usuarioDatamanager.setVerPantalla(ConstantesUtil.PANTALLA_NUEVO);
		usuarioDatamanager.setAccion(ConstantesUtil.ACCION_MODIFICAR);
		usuarioDatamanager.setUsuarioFormul(usuarioSelec.getTblUsuario());
		usuarioDatamanager.getUsuarioFormul().setLoginDTO(
				usuarioSelec.getLoginDTO());
		usuarioDatamanager.setTblSectorUsuarioFormul(usuarioSelec);

	}

	public void eliminar(TblSectorUsuario usuarioSelec) {
		List<TblSectorUsuario> list = sectorUsuarioDTOInterface
				.buscarUser(usuarioSelec.getIdUsu());

		sectorUsuarioDTOInterface.eliminarDelSec(usuarioSelec.getIdUsu(),
				usuarioSelec.getIdSec());

		if (list.isEmpty()) {
			usuarioDTOInterface.remove(usuarioSelec.getTblUsuario());
		}

		JsfUtil.addMessage(ConstantesUtil.MSJ_INFO,
				"Usuario eliminado con �xito");

		if (loginDatamanager.getLoginObj().getIdValuenivel() != ConstantesUtil.NIVEL_SUPER_ADMI) {
			usuarioDatamanager.setSectorUsuarioList(sectorUsuarioDTOInterface
					.findBySector(loginDatamanager.getSectorSeleSec()
							.getIdSec()));
		} else {
			usuarioDatamanager.setSectorUsuarioList(sectorUsuarioDTOInterface
					.findAll());
		}

	}

	public Boolean validarContrasena() {

		if (null == usuarioDatamanager.getContrasena()
				|| usuarioDatamanager.getContrasena().equals(
						ConstantesUtil.CADENA_VACIA)) {
			JsfUtil.addMessage(ConstantesUtil.MSJ_ERROR,
					"Contrase�a y confirmaci�n son requeridas");
			return true;
		}
		return false;
	}

	public void buscarUsuario(AjaxBehaviorEvent event) {
		if (!usuarioDatamanager.getAccion().equals(ConstantesUtil.ACCION_MODIFICAR)) {
			usuarioDatamanager.setExistUserObj(usuarioDTOInterface
					.findByCedula(usuarioDatamanager.getUsuarioFormul()
							.getCedUsu()));

			if (null != usuarioDatamanager.getExistUserObj()) {
				usuarioDatamanager.setExistUser(Boolean.TRUE);
				usuarioDatamanager.setUsuarioFormul(usuarioDatamanager
						.getExistUserObj());
			} else {
				usuarioDatamanager.setExistUser(Boolean.FALSE);
			}
		}
	}

	public usuarioDatamanager getUsuarioDatamanager() {
		return usuarioDatamanager;
	}

	public void setUsuarioDatamanager(usuarioDatamanager usuarioDatamanager) {
		this.usuarioDatamanager = usuarioDatamanager;
	}

	public loginDatamanager getLoginDatamanager() {
		return loginDatamanager;
	}

	public void setLoginDatamanager(loginDatamanager loginDatamanager) {
		this.loginDatamanager = loginDatamanager;
	}

}
