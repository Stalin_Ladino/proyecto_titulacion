package com.ec.ups.controlador;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.ec.ups.common.controlador.CommonControlador;
import com.ec.ups.datamanager.consumoDatamanager;
import com.ec.ups.datamanager.loginDatamanager;
import com.ec.ups.ejb.interfac.impl.consumoDTOInterface;
import com.ec.ups.ejb.interfac.impl.medidoresDTOInterface;
import com.ec.ups.ejb.interfac.impl.regionalDTOInterface;
import com.ec.ups.jpa.entidades.TblConsumo;
import com.ec.ups.jpa.entidades.TblMedidor;
import com.ec.ups.util.ConstantesUtil;
import com.facturacion.util.web.JsfUtil;

@ManagedBean(name = "consumoController")
@ViewScoped
public class consumoController extends CommonControlador {

	private static final long serialVersionUID = 1L;
	private Boolean init = Boolean.TRUE;

	@ManagedProperty(value = "#{consumoDatamanager}")
	private consumoDatamanager consumoDatamanager;

	@ManagedProperty(value = "#{loginDatamanager}")
	private loginDatamanager loginDatamanager;

	@EJB
	private regionalDTOInterface regionalDTOInterface;

	@EJB
	private consumoDTOInterface consumoDTOInterface;

	@EJB
	private medidoresDTOInterface medidoresDTOInterface;

	@Override
	public void inicializarDatos() {
		if (init) {
			init = Boolean.FALSE;
			consumoDatamanager
					.setVerPantalla(ConstantesUtil.PANTALLA_CONSULTAR);

			try {
				this.returnPage();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			consumoDatamanager.setConsumoList(consumoDTOInterface
					.findAllBySector(loginDatamanager.getSectorSeleSec()
							.getIdSec()));
			consumoDatamanager.setConsumoFormul(new TblConsumo());
			consumoDatamanager.setConsumoSelect(new TblConsumo());

		}

	}

	public void returnPage() throws IOException {
		if (null == loginDatamanager.getLoginObj()) {
			JsfUtil.removeManagedBean("loginDataManager");
			JsfUtil.invalidateSession();
			FacesContext fc = FacesContext.getCurrentInstance();
			fc.getExternalContext().redirect(
					"/SistemaFacturacion/page/index2.jsf");
		}
	}

	public void crearConsumo() throws Exception {
		if (!consumoDatamanager.getAccion().equals(
				ConstantesUtil.ACCION_MODIFICAR)) {

			consumoDatamanager.getConsumoFormul().setIdSec(
					loginDatamanager.getSectorSeleSec().getIdSec());
			TblConsumo consumo = consumoDatamanager.getConsumoFormul();
			TblMedidor medidor = medidoresDTOInterface.findMedidorParaConsumo(
					consumo.getIdCodMed(), consumo.getIdSec());

			if (null != medidor) {
				Date today = new Date();
				Date ultimoDiaMes = this.ponerDiasFechaFinMes(today);
				Date primerDiaMes = this.ponerDiasFechaInicioMes(today);

				SimpleDateFormat formatoDeFecha = new SimpleDateFormat(
						"yyyy/MM/dd");
				String primerDia = formatoDeFecha.format(primerDiaMes);
				String ultimoDia = formatoDeFecha.format(ultimoDiaMes);

				List<TblConsumo> resp = consumoDTOInterface.buscarConsMed(
						consumo.getIdCodMed(), primerDia, ultimoDia);

				if (null != resp && !resp.isEmpty()) {
					for (TblConsumo list : resp) {
						consumoDTOInterface.remove(list);
					}
				}

				consumo.setIdMed(medidor.getIdMed());
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
				String fechaString = formatter.format(today);
				consumo.setFechaReg(today);
				consumo.setEstadoFacturado(Boolean.FALSE);
				consumoDTOInterface.create(consumo);
				consumo.setNomUser(medidor.getTblUsuario().getNomUsu());
				consumo.setCedUser(medidor.getTblUsuario().getCedUsu());
				consumo.setFechaConsumo(fechaString);
				consumoDatamanager
						.setVerPantalla(ConstantesUtil.PANTALLA_CONSULTAR);
				JsfUtil.addMessage(ConstantesUtil.MSJ_INFO,
						"Registro guardado con �xito");
				consumoDatamanager.setConsumoList(consumoDTOInterface
						.findAllBySector(loginDatamanager.getSectorSeleSec()
								.getIdSec()));
			} else {
				JsfUtil.addMessage(ConstantesUtil.MSJ_INFO,
						"Medidor no registrado");
				consumo = null;
			}
		} else {
			consumoDTOInterface.edit(consumoDatamanager.getConsumoFormul());
			consumoDatamanager.setConsumoList(consumoDTOInterface
					.findAllBySector(loginDatamanager.getSectorSeleSec()
							.getIdSec()));
			consumoDatamanager
					.setVerPantalla(ConstantesUtil.PANTALLA_CONSULTAR);
			JsfUtil.addMessage(ConstantesUtil.MSJ_INFO,
					"Registro modificado con �xito");
		}
	}

	public Date ponerDiasFechaFinMes(Date fecha) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fecha); // Configuramos la fecha que se recibe
		calendar.set(calendar.get(Calendar.YEAR),
				(calendar.get(Calendar.MONTH)),
				calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		return calendar.getTime(); // Devuelve el objeto Date con los nuevos
									// d�as a�adidos

	}

	public Date ponerDiasFechaInicioMes(Date fecha) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fecha); // Configuramos la fecha que se recibe
		calendar.set(calendar.get(Calendar.YEAR),
				(calendar.get(Calendar.MONTH)),
				calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
		return calendar.getTime(); // Devuelve el objeto Date con los nuevos
									// d�as a�adidos

	}

	public void accion(String pantalla) {
		consumoDatamanager.setVerPantalla(pantalla);
		consumoDatamanager.setAccion(ConstantesUtil.ACCION_NUEVO);
		consumoDatamanager.setConsumoSelect(new TblConsumo());

	}

	public void modificar(TblConsumo consumoSelec) {
		consumoDatamanager.setVerPantalla(ConstantesUtil.PANTALLA_NUEVO);
		consumoDatamanager.setAccion(ConstantesUtil.ACCION_MODIFICAR);
		consumoDatamanager.setConsumoFormul(consumoSelec);
		consumoDatamanager.setConsumoSelect(new TblConsumo());

	}

	public void eliminar(TblConsumo consumoSelec) {
		consumoDTOInterface.remove(consumoSelec);
		JsfUtil.addMessage(ConstantesUtil.MSJ_INFO,
				"Registro eliminado con �xito");
		consumoDatamanager
				.setConsumoList(consumoDTOInterface
						.findAllBySector(loginDatamanager.getSectorSeleSec()
								.getIdSec()));

	}

	public void consultar() {
		SimpleDateFormat formatoDeFecha = new SimpleDateFormat("yyyy/MM/dd");
		String primerDia = formatoDeFecha.format(consumoDatamanager
				.getFechaIni());
		String ultimoDia = formatoDeFecha.format(consumoDatamanager
				.getFechaFin());
		consumoDatamanager.setConsumoList(consumoDTOInterface
				.buscarConsMedidorFech(primerDia, ultimoDia, loginDatamanager
						.getSectorSeleSec().getIdSec()));
	}

	public consumoDatamanager getconsumoDatamanager() {
		return consumoDatamanager;
	}

	public void setconsumoDatamanager(consumoDatamanager consumoDatamanager) {
		this.consumoDatamanager = consumoDatamanager;
	}

	public loginDatamanager getLoginDatamanager() {
		return loginDatamanager;
	}

	public void setLoginDatamanager(loginDatamanager loginDatamanager) {
		this.loginDatamanager = loginDatamanager;
	}

}
