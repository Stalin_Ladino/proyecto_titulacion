package com.ec.ups.controlador;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.ec.ups.common.controlador.CommonControlador;
import com.ec.ups.datamanager.loginDatamanager;
import com.ec.ups.datamanager.medidoresDatamanager;
import com.ec.ups.ejb.interfac.impl.medidoresDTOInterface;
import com.ec.ups.ejb.interfac.impl.secComDTOInterface;
import com.ec.ups.ejb.interfac.impl.serviciosDTOInterface;
import com.ec.ups.ejb.interfac.impl.usuarioDTOInterface;
import com.ec.ups.jpa.entidades.TblMedidor;
import com.ec.ups.jpa.entidades.secComDTO;
import com.ec.ups.jpa.entidades.usuarioDTO;
import com.ec.ups.util.ConstantesUtil;
import com.facturacion.util.web.JsfUtil;

@ManagedBean(name = "medidoresController")
@ViewScoped
public class medidoresController extends CommonControlador {

	private static final long serialVersionUID = 1L;

	@ManagedProperty(value = "#{medidoresDatamanager}")
	private medidoresDatamanager medidoresDatamanager;

	private Boolean init = Boolean.TRUE;

	@EJB
	private usuarioDTOInterface usuarioDTOInterface;

	@EJB
	private secComDTOInterface secComDTOInterface;

	@EJB
	private serviciosDTOInterface serviciosDTOInterface;

	@EJB
	private medidoresDTOInterface medidoresDTOInterface;

	@ManagedProperty(value = "#{loginDatamanager}")
	private loginDatamanager loginDatamanager;

	@Override
	public void inicializarDatos() {

		if (init) {
			init = Boolean.FALSE;
			try {
				this.returnPage();
			} catch (IOException e) {
				e.printStackTrace();
			}
			medidoresDatamanager.setMedidorFormul(new TblMedidor());
			medidoresDatamanager.setUsuarioSelect(new usuarioDTO());
			medidoresDatamanager.setAccion(ConstantesUtil.ACCION_SIN_DATOS);

			if (loginDatamanager.getLoginObj().getIdValuenivel() != ConstantesUtil.NIVEL_SUPER_ADMI) {
				List<secComDTO> list = new ArrayList<secComDTO>();

				list.add(secComDTOInterface.findById(loginDatamanager
						.getSectorSeleSec().getIdSec()));
				medidoresDatamanager.setSecComList(list);
			} else {
				medidoresDatamanager
						.setSecComList(secComDTOInterface.findAll());
			}

			medidoresDatamanager.setServiciosList(serviciosDTOInterface
					.findAll());
			medidoresDatamanager.setHabilitarNuevo(Boolean.TRUE);
			medidoresDatamanager
					.setListMedidoresAll(new ArrayList<TblMedidor>());
			medidoresDatamanager.setCambioSector(Boolean.FALSE);
			medidoresDatamanager.setCambioServicio(Boolean.FALSE);
		}
	}

	public void returnPage() throws IOException {
		if (null == loginDatamanager.getLoginObj()) {
			JsfUtil.removeManagedBean("loginDataManager");
			JsfUtil.invalidateSession();
			FacesContext fc = FacesContext.getCurrentInstance();
			fc.getExternalContext().redirect(
					"/SistemaFacturacion/page/index2.jsf");
		}
	}

	public void consultar() {

		if (null == medidoresDatamanager.getCedula()
				|| medidoresDatamanager.getCedula().equals(
						ConstantesUtil.CADENA_VACIA)) {
			if (!medidoresDatamanager.getAccion().equals(
					ConstantesUtil.ACCION_NUEVO)) {
				medidoresDatamanager.setAccion(ConstantesUtil.ACCION_CONSULTAR);
			}

			if (loginDatamanager.getLoginObj().getIdValuenivel() != ConstantesUtil.NIVEL_SUPER_ADMI) {

				medidoresDatamanager.setListMedidores(medidoresDTOInterface
						.findAllBySector(loginDatamanager.getSectorSeleSec()
								.getIdSec()));
			} else {
				medidoresDatamanager.setListMedidores(medidoresDTOInterface
						.findAll());
			}

		} else {

			usuarioDTO usuario = new usuarioDTO();

			if (loginDatamanager.getLoginObj().getIdValuenivel() != ConstantesUtil.NIVEL_SUPER_ADMI) {
				usuario = usuarioDTOInterface.buscarByCedulaSector(
						medidoresDatamanager.getCedula(),
						loginDatamanager.getSectorSeleSec().getIdSec()).size() > 0 ? usuarioDTOInterface
						.buscarByCedulaSector(medidoresDatamanager.getCedula(),
								loginDatamanager.getSectorSeleSec().getIdSec())
						.get(0) : null;
			} else {
				usuario = usuarioDTOInterface.findByCedula(medidoresDatamanager
						.getCedula());
			}

			if (null == usuario) {
				medidoresDatamanager.setHabilitarNuevo(Boolean.TRUE);
				JsfUtil.addMessage(ConstantesUtil.MSJ_INFO,
						"N�mero de c�dula no registrado.");
				medidoresDatamanager.setUsuarioSelect(new usuarioDTO());
				medidoresDatamanager.setMedidorFormul(new TblMedidor());
				medidoresDatamanager.setAccion(ConstantesUtil.ACCION_SIN_DATOS);
			} else {
				medidoresDatamanager.setUserSelectBuscar(usuario);
				medidoresDatamanager.setHabilitarNuevo(Boolean.FALSE);
				if (!medidoresDatamanager.getAccion().equals(
						ConstantesUtil.ACCION_NUEVO)) {
					medidoresDatamanager
							.setAccion(ConstantesUtil.ACCION_CONSULTAR);
				}
				medidoresDatamanager.setUsuarioSelect(usuario);

				List<TblMedidor> medidor = new ArrayList<TblMedidor>();

				if (loginDatamanager.getLoginObj().getIdValuenivel() != ConstantesUtil.NIVEL_SUPER_ADMI) {
					medidor = medidoresDTOInterface.buscarPorIdSector(usuario
							.getIdUsu(), loginDatamanager.getSectorSeleSec()
							.getIdSec());
				} else {
					medidor = medidoresDTOInterface.buscarPorIdUser(usuario
							.getIdUsu());
				}

				if (medidor.size() == 0) {
					JsfUtil.addMessage(ConstantesUtil.MSJ_INFO,
							"No existe medidor registrado para el usuario : "
									+ usuario.getCedUsu());
					medidoresDatamanager
							.setListMedidores(new ArrayList<TblMedidor>());
				} else {
					medidoresDatamanager.setListMedidores(medidor);
				}
			}
		}
	}

	public void accion(String pantalla) {
		if (pantalla.equals(ConstantesUtil.ACCION_NUEVO)) {
			if (loginDatamanager.getLoginObj().getIdValuenivel() != ConstantesUtil.NIVEL_SUPER_ADMI) {
				List<secComDTO> list = new ArrayList<secComDTO>();
				list.add(secComDTOInterface.findById(loginDatamanager
						.getSectorSeleSec().getIdSec()));
				medidoresDatamanager.setSecComList(list);
			} else {
				medidoresDatamanager
						.setSecComList(secComDTOInterface.findAll());
			}
		}
		if (pantalla.equals(ConstantesUtil.ACCION_CONSULTAR)) {
			consultar();
		}
		medidoresDatamanager.setVerPantalla(pantalla);
		medidoresDatamanager.setAccion(pantalla);
		medidoresDatamanager.setMedidorFormul(new TblMedidor());

	}

	public void modificar(TblMedidor medidorSe) {
		medidoresDatamanager.setVerPantalla(ConstantesUtil.PANTALLA_NUEVO);
		medidoresDatamanager.setAccion(ConstantesUtil.ACCION_MODIFICAR);
		if (null != medidorSe.getTblUsuario()) {
			medidoresDatamanager.setUsuarioSelect(medidorSe.getTblUsuario());
		}
		medidoresDatamanager.setMedidorFormul(medidorSe);
		String codMedi = medidorSe.getIdCod();
		medidoresDatamanager.setValidarCodMedidor(codMedi);

	}

	public void eliminar(TblMedidor medidorSe) {
		medidoresDTOInterface.remove(medidorSe);
		JsfUtil.addMessage(ConstantesUtil.MSJ_INFO,
				"Medidor eliminado con �xito");
		medidoresDatamanager.setAccion(ConstantesUtil.ACCION_MODIFICAR);
		consultar();
	}

	public void guardar() {

		if (medidoresDatamanager.getAccion()
				.equals(ConstantesUtil.ACCION_NUEVO)) {
			// if(validarGuardado()){
			// return;
			// }

			if (validarMedidor()) {
				return;
			}

			usuarioDTO user = medidoresDatamanager.getUsuarioSelect();
			medidoresDatamanager.getMedidorFormul().setId_usu(user.getIdUsu());
			medidoresDTOInterface.create(medidoresDatamanager
					.getMedidorFormul());
			medidoresDatamanager.setAccion(ConstantesUtil.ACCION_CONSULTAR);
			JsfUtil.addMessage(ConstantesUtil.MSJ_INFO,
					"Medidor registrado con �xito");

		} else {

			// if (medidoresDatamanager.getCambioSector() == Boolean.TRUE
			// || medidoresDatamanager.getCambioSector() == Boolean.TRUE) {
			// if(validarGuardado()){
			// return;
			// }
			// }

			if (!medidoresDatamanager.getValidarCodMedidor().equals(
					medidoresDatamanager.getMedidorFormul().getIdCod())) {
				if (validarMedidor()) {
					return;
				}
			}

			usuarioDTO user = medidoresDatamanager.getUsuarioSelect();
			medidoresDatamanager.getMedidorFormul().setId_usu(user.getIdUsu());

			medidoresDTOInterface.edit(medidoresDatamanager.getMedidorFormul());
			medidoresDatamanager.setAccion(ConstantesUtil.ACCION_CONSULTAR);
			JsfUtil.addMessage(ConstantesUtil.MSJ_INFO,
					"Medidor modificado con �xito");
			medidoresDatamanager.setCambioSector(Boolean.FALSE);
			medidoresDatamanager.setCambioServicio(Boolean.FALSE);
		}

		consultar();
	}

	public Boolean validarGuardado() {

		List<TblMedidor> medidor = medidoresDTOInterface
				.buscarPorIdUser(medidoresDatamanager.getUserSelectBuscar()
						.getIdUsu());

		for (TblMedidor objList : medidor) {
			if (objList.getId_sec().equals(
					medidoresDatamanager.getMedidorFormul().getId_sec())) {
				if (objList.getId_serv().equals(
						medidoresDatamanager.getMedidorFormul().getId_serv())) {
					JsfUtil.addMessage(
							ConstantesUtil.MSJ_INFO,
							"El usuario ya tiene un medidor asignado para el sector y servicio seleccionado");
					return true;
				}
			}
		}
		return false;

	}

	public Boolean validarMedidor() {

		TblMedidor medidor = medidoresDTOInterface
				.findCodMedidor(medidoresDatamanager.getMedidorFormul()
						.getIdCod());

		if (null != medidor) {
			JsfUtil.addMessage(ConstantesUtil.MSJ_ERROR,
					"El c�digo del medidor ya esta registrado");
			return true;
		}
		return false;
	}

	public void cambioSector() {
		medidoresDatamanager.setCambioSector(Boolean.TRUE);
	}

	public void cambioServicio() {
		medidoresDatamanager.setCambioServicio(Boolean.TRUE);
	}

	public medidoresDatamanager getMedidoresDatamanager() {
		return medidoresDatamanager;
	}

	public void setMedidoresDatamanager(
			medidoresDatamanager medidoresDatamanager) {
		this.medidoresDatamanager = medidoresDatamanager;
	}

	public loginDatamanager getLoginDatamanager() {
		return loginDatamanager;
	}

	public void setLoginDatamanager(loginDatamanager loginDatamanager) {
		this.loginDatamanager = loginDatamanager;
	}

}
