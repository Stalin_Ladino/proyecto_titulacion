/*

 * Copyright 2014 Kruger
 * Todos los derechos reservados
 */
package com.ec.ups.common.controlador;

import java.io.IOException;
import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlForm;
import javax.faces.context.FacesContext;

import com.ec.ups.datamanager.loginDatamanager;
import com.facturacion.util.web.JsfUtil;

/**
 * Controlador con operaciones comunes a todos los controladores de la aplicacion
 * 
 * @author Kruger - Santiago Cabrera M.
 * 
 */
@ManagedBean(name="commonControlador")
@ViewScoped
public abstract class CommonControlador implements Serializable {

	@ManagedProperty(value = "#{loginDatamanager}") 
	private loginDatamanager loginDatamanager;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	/**
	 * Formulario
	 */
	private HtmlForm formul;

	/**
	 * Se encarga de inicializar los datos de una p&aacute;gina (resetear valores, borrar datos, etc)
	 * 
	 * @author msatan
	 */
	public abstract void inicializarDatos();

	/**
	 * Inicializa el formulario
	 * @throws IOException 
	 */
	public HtmlForm getFormul() throws IOException {
//		if(null!=loginDatamanager){
			inicializarDatos();
//		} else {
//			JsfUtil.removeManagedBean("loginDataManager");
//			JsfUtil.invalidateSession();
//			FacesContext fc = FacesContext.getCurrentInstance();
//			fc.getExternalContext().redirect(
//					"/SistemaFacturacionUPS/index");
//		}
		
		// validarPermisosPantalla();
		// fijarPermisosPagina();
		return this.formul;
	}


	public loginDatamanager getLoginDatamanager() {
		return loginDatamanager;
	}

	public void setLoginDatamanager(loginDatamanager loginDatamanager) {
		this.loginDatamanager = loginDatamanager;
	}
	
}
