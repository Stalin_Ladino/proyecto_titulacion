/*
 * Copyright 2014 Kruger
 * Todos los derechos reservados
 */
package com.ec.ups.datamanager;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.ec.ups.jpa.entidades.ResultadoFactura;
import com.ec.ups.jpa.entidades.TblMedidor;
import com.ec.ups.jpa.entidades.TblMulta;
import com.ec.ups.jpa.entidades.TblServicio;
import com.ec.ups.jpa.entidades.TblUsuarioMulta;
import com.ec.ups.jpa.entidades.secComDTO;
import com.ec.ups.jpa.entidades.usuarioDTO;

/**
 * Guarda los datos de la pantalla carga depositos
 * @author sladino
 *
 */
@ManagedBean(name="facturacionDatamanager")
@SessionScoped
public class facturacionDatamanager implements Serializable {


	private static final long serialVersionUID = 1L;
	

	
	private TblMulta mulFormul;
	
	private List<TblMulta> mulList;
	
	private TblMulta mulSeleccion;

	private String verPantalla;
	
	private String accion;
	
	private List<secComDTO> secComList;
	
	private List<TblMedidor> MedidorList;
	
	private TblMedidor medidorSelect;
	
	private secComDTO secComSelect;
	
	private String cedula;
	
	private usuarioDTO usuarioSector;
	
	private List<TblServicio> serviciosList;
	
	private TblServicio servicioSelect;
	
	private List<usuarioDTO> usuarioList;
	
	private usuarioDTO usuarioSelect;
	
	private Date fechaIni;
	
	private Date fechaFin;
	
	private List<ResultadoFactura> resultadoList;
	
	private ResultadoFactura resultadoSelect;
	
	private HashMap<Integer, TblServicio> mapServicios= new HashMap<>();
	
	private Double valorPagar;
	
	private List<TblMulta> multasList;
	
	private TblMulta multaSelect;
	
	private List<TblMulta> multasListSel;
	
	private Double valorMul;
	
	private Double valorTotal;
	
	private List<TblUsuarioMulta> usuarioMultasList;
	
	private TblUsuarioMulta usuarioMultaSelect;
	
	private String fechIniMulta;
	
	private String fechFinMulta;
	
	private String tarifaMedidor;
	
	private String tarifaExcedente;
	
	private Double calculoExcedente;
	
	private Double valorPagarExcedente;
	
	private Double valorPagarMedidor;
	
	private Long consumoTotal;
	
	public TblMulta getmulFormul() {
		return mulFormul;
	}

	public void setmulFormul(TblMulta mulFormul) {
		this.mulFormul = mulFormul;
	}

	public List<TblMulta> getmulList() {
		return mulList;
	}

	public void setmulList(List<TblMulta> mulList) {
		this.mulList = mulList;
	}

	public TblMulta getmulSeleccion() {
		return mulSeleccion;
	}

	public void setmulSeleccion(TblMulta mulSeleccion) {
		this.mulSeleccion = mulSeleccion;
	}

	public String getVerPantalla() {
		return verPantalla;
	}

	public void setVerPantalla(String verPantalla) {
		this.verPantalla = verPantalla;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public List<secComDTO> getSecComList() {
		return secComList;
	}

	public void setSecComList(List<secComDTO> secComList) {
		this.secComList = secComList;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public usuarioDTO getUsuarioSector() {
		return usuarioSector;
	}

	public void setUsuarioSector(usuarioDTO usuarioSector) {
		this.usuarioSector = usuarioSector;
	}

	public secComDTO getSecComSelect() {
		return secComSelect;
	}

	public void setSecComSelect(secComDTO secComSelect) {
		this.secComSelect = secComSelect;
	}

	public List<TblMedidor> getMedidorList() {
		return MedidorList;
	}

	public void setMedidorList(List<TblMedidor> medidorList) {
		MedidorList = medidorList;
	}

	public TblMedidor getMedidorSelect() {
		return medidorSelect;
	}

	public void setMedidorSelect(TblMedidor medidorSelect) {
		this.medidorSelect = medidorSelect;
	}

	public List<TblServicio> getServiciosList() {
		return serviciosList;
	}

	public void setServiciosList(List<TblServicio> serviciosList) {
		this.serviciosList = serviciosList;
	}

	public TblServicio getServicioSelect() {
		return servicioSelect;
	}

	public void setServicioSelect(TblServicio servicioSelect) {
		this.servicioSelect = servicioSelect;
	}

	public List<usuarioDTO> getUsuarioList() {
		if(usuarioList==null){
			usuarioList = new ArrayList<usuarioDTO>(); 
			};
		return usuarioList;
	}

	public void setUsuarioList(List<usuarioDTO> usuarioList) {
		this.usuarioList = usuarioList;
	}

	public usuarioDTO getUsuarioSelect() {
		return usuarioSelect;
	}

	public void setUsuarioSelect(usuarioDTO usuarioSelect) {
		this.usuarioSelect = usuarioSelect;
	}

	public Date getFechaIni() {
		return fechaIni;
	}

	public void setFechaIni(Date fechaIni) {
		this.fechaIni = fechaIni;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public List<ResultadoFactura> getResultadoList() {
		return resultadoList;
	}

	public void setResultadoList(List<ResultadoFactura> resultadoList) {
		this.resultadoList = resultadoList;
	}

	public ResultadoFactura getResultadoSelect() {
		return resultadoSelect;
	}

	public void setResultadoSelect(ResultadoFactura resultadoSelect) {
		this.resultadoSelect = resultadoSelect;
	}

	public HashMap<Integer, TblServicio> getMapServicios() {
		return mapServicios;
	}

	public void setMapServicios(HashMap<Integer, TblServicio> mapServicios) {
		this.mapServicios = mapServicios;
	}

	public Double getValorPagar() {
		return valorPagar;
	}

	public void setValorPagar(Double valorPagar) {
		this.valorPagar = valorPagar;
	}

	public List<TblMulta> getMultasList() {
		return multasList;
	}

	public void setMultasList(List<TblMulta> multasList) {
		this.multasList = multasList;
	}

	public TblMulta getMultaSelect() {
		return multaSelect;
	}

	public void setMultaSelect(TblMulta multaSelect) {
		this.multaSelect = multaSelect;
	}

	public List<TblMulta> getMultasListSel() {
		return multasListSel;
	}

	public void setMultasListSel(List<TblMulta> multasListSel) {
		this.multasListSel = multasListSel;
	}

	public Double getValorMul() {
		return valorMul;
	}

	public void setValorMul(Double valorMul) {
		this.valorMul = valorMul;
	}

	public Double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}

	public List<TblUsuarioMulta> getUsuarioMultasList() {
		return usuarioMultasList;
	}

	public void setUsuarioMultasList(List<TblUsuarioMulta> usuarioMultasList) {
		this.usuarioMultasList = usuarioMultasList;
	}

	public TblUsuarioMulta getUsuarioMultaSelect() {
		return usuarioMultaSelect;
	}

	public void setUsuarioMultaSelect(TblUsuarioMulta usuarioMultaSelect) {
		this.usuarioMultaSelect = usuarioMultaSelect;
	}

	public String getFechIniMulta() {
		return fechIniMulta;
	}

	public void setFechIniMulta(String fechIniMulta) {
		this.fechIniMulta = fechIniMulta;
	}

	public String getFechFinMulta() {
		return fechFinMulta;
	}

	public void setFechFinMulta(String fechFinMulta) {
		this.fechFinMulta = fechFinMulta;
	}

	public String getTarifaMedidor() {
		return tarifaMedidor;
	}

	public void setTarifaMedidor(String tarifaMedidor) {
		this.tarifaMedidor = tarifaMedidor;
	}

	public String getTarifaExcedente() {
		return tarifaExcedente;
	}

	public void setTarifaExcedente(String tarifaExcedente) {
		this.tarifaExcedente = tarifaExcedente;
	}

	public Double getCalculoExcedente() {
		return calculoExcedente;
	}

	public void setCalculoExcedente(Double calculoExcedente) {
		this.calculoExcedente = calculoExcedente;
	}

	public Double getValorPagarExcedente() {
		return valorPagarExcedente;
	}

	public void setValorPagarExcedente(Double valorPagarExcedente) {
		this.valorPagarExcedente = valorPagarExcedente;
	}

	public Double getValorPagarMedidor() {
		return valorPagarMedidor;
	}

	public void setValorPagarMedidor(Double valorPagarMedidor) {
		this.valorPagarMedidor = valorPagarMedidor;
	}

	public Long getConsumoTotal() {
		return consumoTotal;
	}

	public void setConsumoTotal(Long consumoTotal) {
		this.consumoTotal = consumoTotal;
	}
	
	
}
