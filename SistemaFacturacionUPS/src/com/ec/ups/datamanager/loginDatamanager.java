/*
 * Copyright 2014 Kruger
 * Todos los derechos reservados
 */
package com.ec.ups.datamanager;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.ec.ups.jpa.entidades.TblPagina;
import com.ec.ups.jpa.entidades.TblSectorUsuario;
import com.ec.ups.jpa.entidades.loginDTO;
import com.ec.ups.jpa.entidades.secComDTO;
import com.ec.ups.jpa.entidades.usuarioDTO;

/**
 * Guarda los datos de la pantalla carga depositos
 * @author sladino
 *
 */
@ManagedBean(name="loginDatamanager")
@SessionScoped
public class loginDatamanager implements Serializable {


	private static final long serialVersionUID = 1L;
	

	
	private loginDTO loginObj;

	private String nombreUsuario;
	
	private String password;
	
	private List<TblPagina> tblPaginaList;
	
	private List<usuarioDTO> listUsuario;
	
	private List<secComDTO> listSectores;
	
	private secComDTO sectorSelcUser;
	
	private secComDTO sectorSeleSec;
	
	private usuarioDTO usuarioSession;
	
	private HashMap<Integer, TblSectorUsuario> mapUserSec;


	public loginDTO getLoginObj() {
		return loginObj;
	}



	public void setLoginObj(loginDTO loginObj) {
		this.loginObj = loginObj;
	}



	public String getNombreUsuario() {
		return nombreUsuario;
	}



	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}



	public String getPassword() {
		return password;
	}



	public void setPassword(String password) {
		this.password = password;
	}



	public List<TblPagina> getTblPaginaList() {
		return tblPaginaList;
	}



	public void setTblPaginaList(List<TblPagina> tblPaginaList) {
		this.tblPaginaList = tblPaginaList;
	}



	public List<usuarioDTO> getListUsuario() {
		return listUsuario;
	}



	public void setListUsuario(List<usuarioDTO> listUsuario) {
		this.listUsuario = listUsuario;
	}



	public List<secComDTO> getListSectores() {
		return listSectores;
	}



	public void setListSectores(List<secComDTO> listSectores) {
		this.listSectores = listSectores;
	}



	public secComDTO getSectorSelcUser() {
		return sectorSelcUser;
	}



	public secComDTO getSectorSeleSec() {
		return sectorSeleSec;
	}



	public void setSectorSeleSec(secComDTO sectorSeleSec) {
		this.sectorSeleSec = sectorSeleSec;
	}



	public void setSectorSelcUser(secComDTO sectorSelcUser) {
	
		this.sectorSelcUser = sectorSelcUser;
	}



	public HashMap<Integer, TblSectorUsuario> getMapUserSec() {
		return mapUserSec;
	}



	public void setMapUserSec(HashMap<Integer, TblSectorUsuario> mapUserSec) {
		this.mapUserSec = mapUserSec;
	}



	public usuarioDTO getUsuarioSession() {
		return usuarioSession;
	}



	public void setUsuarioSession(usuarioDTO usuarioSession) {
		this.usuarioSession = usuarioSession;
	}
	
	
	
}
