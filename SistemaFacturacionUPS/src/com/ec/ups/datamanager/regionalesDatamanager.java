/*
 * Copyright 2014 Kruger
 * Todos los derechos reservados
 */
package com.ec.ups.datamanager;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.ec.ups.jpa.entidades.TblRegional;

/**
 * Guarda los datos de la pantalla carga depositos
 * @author sladino
 *
 */
@ManagedBean(name="regionalesDatamanager")
@SessionScoped
public class regionalesDatamanager implements Serializable {


	private static final long serialVersionUID = 1L;
	

	
	private TblRegional regionalFormul;
	
	private List<TblRegional> regionalList;
	
	private TblRegional regionalSeleccion;

	private String verPantalla;
	
	private String accion;
	
	
	public TblRegional getRegionalFormul() {
		return regionalFormul;
	}

	public void setRegionalFormul(TblRegional regionalFormul) {
		this.regionalFormul = regionalFormul;
	}

	public List<TblRegional> getRegionalList() {
		return regionalList;
	}

	public void setRegionalList(List<TblRegional> regionalList) {
		this.regionalList = regionalList;
	}

	public TblRegional getRegionalSeleccion() {
		return regionalSeleccion;
	}

	public void setRegionalSeleccion(TblRegional regionalSeleccion) {
		this.regionalSeleccion = regionalSeleccion;
	}

	public String getVerPantalla() {
		return verPantalla;
	}

	public void setVerPantalla(String verPantalla) {
		this.verPantalla = verPantalla;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	
	
	
	

}
