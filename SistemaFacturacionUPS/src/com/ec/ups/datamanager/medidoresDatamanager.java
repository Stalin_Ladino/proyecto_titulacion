/*
 * Copyright 2014 Kruger
 * Todos los derechos reservados
 */
package com.ec.ups.datamanager;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.ec.ups.jpa.entidades.TblMedidor;
import com.ec.ups.jpa.entidades.TblServicio;
import com.ec.ups.jpa.entidades.secComDTO;
import com.ec.ups.jpa.entidades.usuarioDTO;

/**
 * Guarda los datos de la pantalla carga depositos
 * @author sladino
 *
 */
@ManagedBean(name="medidoresDatamanager")
@SessionScoped
public class medidoresDatamanager implements Serializable {


	private static final long serialVersionUID = 1L;
	
	private TblMedidor medidorFormul;
	
	private usuarioDTO usuarioSelect;
	
	private String verPantalla;
		
	private List<secComDTO> secComList;
	
	private List<TblServicio> serviciosList;
	
	private String cedula;
	
	private String accion;
	
	private List<TblMedidor> listMedidores;
	
	private List<TblMedidor> listMedidoresAll;
	
	private Boolean habilitarNuevo;
	
	private usuarioDTO userSelectBuscar;
	
	private Boolean cambioSector;
	
	private Boolean cambioServicio;
	
	private String validarCodMedidor;

	
	public String getVerPantalla() {
		return verPantalla;
	}

	public void setVerPantalla(String verPantalla) {
		this.verPantalla = verPantalla;
	}

	public TblMedidor getMedidorFormul() {
		return medidorFormul;
	}

	public void setMedidorFormul(TblMedidor medidorFormul) {
		this.medidorFormul = medidorFormul;
	}

	public usuarioDTO getUsuarioSelect() {
		if(null==usuarioSelect){
			usuarioSelect=new usuarioDTO();
		}
		return usuarioSelect;
	}

	public void setUsuarioSelect(usuarioDTO usuarioSelect) {
		this.usuarioSelect = usuarioSelect;
	}

	public List<secComDTO> getSecComList() {
		return secComList;
	}

	public void setSecComList(List<secComDTO> secComList) {
		this.secComList = secComList;
	}

	public List<TblServicio> getServiciosList() {
		return serviciosList;
	}

	public void setServiciosList(List<TblServicio> serviciosList) {
		this.serviciosList = serviciosList;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public List<TblMedidor> getListMedidores() {
		return listMedidores;
	}

	public void setListMedidores(List<TblMedidor> listMedidores) {
		this.listMedidores = listMedidores;
	}

	public Boolean getHabilitarNuevo() {
		return habilitarNuevo;
	}

	public void setHabilitarNuevo(Boolean habilitarNuevo) {
		this.habilitarNuevo = habilitarNuevo;
	}

	public List<TblMedidor> getListMedidoresAll() {
		return listMedidoresAll;
	}

	public void setListMedidoresAll(List<TblMedidor> listMedidoresAll) {
		this.listMedidoresAll = listMedidoresAll;
	}

	public usuarioDTO getUserSelectBuscar() {
		return userSelectBuscar;
	}

	public void setUserSelectBuscar(usuarioDTO userSelectBuscar) {
		this.userSelectBuscar = userSelectBuscar;
	}

	public Boolean getCambioSector() {
		return cambioSector;
	}

	public void setCambioSector(Boolean cambioSector) {
		this.cambioSector = cambioSector;
	}

	public Boolean getCambioServicio() {
		return cambioServicio;
	}

	public void setCambioServicio(Boolean cambioServicio) {
		this.cambioServicio = cambioServicio;
	}

	public String getValidarCodMedidor() {
		return validarCodMedidor;
	}

	public void setValidarCodMedidor(String validarCodMedidor) {
		this.validarCodMedidor = validarCodMedidor;
	}
}
