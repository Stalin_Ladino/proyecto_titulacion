/*
 * Copyright 2014 Kruger
 * Todos los derechos reservados
 */
package com.ec.ups.datamanager;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.ec.ups.jpa.entidades.TblExcedente;
import com.ec.ups.jpa.entidades.TblPagina;
import com.ec.ups.jpa.entidades.TblParametro;
import com.ec.ups.jpa.entidades.TblServicio;
import com.ec.ups.jpa.entidades.secComDTO;

/**
 * Guarda los datos de la pantalla carga depositos
 * @author sladino
 *
 */
@ManagedBean(name="parametrizacionDatamanager")
@SessionScoped
public class parametrizacionDatamanager implements Serializable {


	private static final long serialVersionUID = 1L;

	private List<TblPagina> tblPaginaList;

	private TblParametro parametroObjs;
	
	private List<TblParametro> listParametros;
	
	private String accion;
		
	private String verPantalla;
	
	private List<secComDTO> secComList;
	
	private List<TblServicio> serviciosList;
	
	private List<TblExcedente> excedenteList;
	
	private TblExcedente excedenteObj;
	
	private TblExcedente excedenteFormul;
	
	private HashMap<Integer, TblExcedente> excedenteMap;
	
	public TblParametro getParametroObjs() {
		if(null==parametroObjs){
			parametroObjs=new TblParametro();
			parametroObjs.setSecComDTO(new secComDTO());
			
		}
		return parametroObjs;
	}

	public void setParametroObjs(TblParametro parametroObjs) {
		this.parametroObjs = parametroObjs;
	}

	public List<TblPagina> getTblPaginaList() {
		return tblPaginaList;
	}

	public void setTblPaginaList(List<TblPagina> tblPaginaList) {
		this.tblPaginaList = tblPaginaList;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public String getVerPantalla() {
		return verPantalla;
	}

	public void setVerPantalla(String verPantalla) {
		this.verPantalla = verPantalla;
	}

	public List<secComDTO> getSecComList() {
		return secComList;
	}

	public void setSecComList(List<secComDTO> secComList) {
		this.secComList = secComList;
	}

	public List<TblParametro> getListParametros() {
		return listParametros;
	}

	public void setListParametros(List<TblParametro> listParametros) {
		this.listParametros = listParametros;
	}

	public List<TblServicio> getServiciosList() {
		return serviciosList;
	}

	public void setServiciosList(List<TblServicio> serviciosList) {
		this.serviciosList = serviciosList;
	}

	public List<TblExcedente> getExcedenteList() {
		return excedenteList;
	}

	public void setExcedenteList(List<TblExcedente> excedenteList) {
		this.excedenteList = excedenteList;
	}

	public TblExcedente getExcedenteObj() {
		return excedenteObj;
	}

	public void setExcedenteObj(TblExcedente excedenteObj) {
		this.excedenteObj = excedenteObj;
	}

	public TblExcedente getExcedenteFormul() {
		return excedenteFormul;
	}

	public void setExcedenteFormul(TblExcedente excedenteFormul) {
		this.excedenteFormul = excedenteFormul;
	}

	public HashMap<Integer, TblExcedente> getExcedenteMap() {
		return excedenteMap;
	}

	public void setExcedenteMap(HashMap<Integer, TblExcedente> excedenteMap) {
		this.excedenteMap = excedenteMap;
	}
	
	
}
