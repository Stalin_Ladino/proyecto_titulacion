/*
 * Copyright 2014 Kruger
 * Todos los derechos reservados
 */
package com.ec.ups.datamanager;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.ec.ups.jpa.entidades.TblConsumo;

/**
 * Guarda los datos de la pantalla carga depositos
 * @author sladino
 *
 */
@ManagedBean(name="consumoDatamanager")
@SessionScoped
public class consumoDatamanager implements Serializable {


	private static final long serialVersionUID = 1L;
	

	
	private TblConsumo consumoFormul;
	
	private List<TblConsumo> consumoList;
	
	private TblConsumo consumoSelect;

	private String verPantalla;
	
	private String accion;
	
	private Date fechaIni;
	
	private Date fechaFin;


	public TblConsumo getConsumoFormul() {
		return consumoFormul;
	}

	public void setConsumoFormul(TblConsumo consumoFormul) {
		this.consumoFormul = consumoFormul;
	}

	public String getVerPantalla() {
		return verPantalla;
	}

	public void setVerPantalla(String verPantalla) {
		this.verPantalla = verPantalla;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public List<TblConsumo> getConsumoList() {
		return consumoList;
	}

	public void setConsumoList(List<TblConsumo> consumoList) {
		this.consumoList = consumoList;
	}

	public TblConsumo getConsumoSelect() {
		return consumoSelect;
	}

	public void setConsumoSelect(TblConsumo consumoSelect) {
		this.consumoSelect = consumoSelect;
	}

	public Date getFechaIni() {
		return fechaIni;
	}

	public void setFechaIni(Date fechaIni) {
		this.fechaIni = fechaIni;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	
	
	
}
