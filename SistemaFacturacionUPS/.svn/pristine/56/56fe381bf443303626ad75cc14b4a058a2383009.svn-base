/*
 * Copyright 2013 Kruger
 * Todos los derechos reservados
 */
package com.gpf.util.web;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.context.RequestContext;

import com.ec.gpf.util.ConstantesUtil;


/**
 * Maneja los metodos para gestionar el contexto y beans de la instancia actual Jsf
 *  Creado: 21/09/2016
 * @author sladino
 *
 */
public class JsfUtil {

	/**
	 * Instancia singleton
	 */
	private static final JsfUtil INSTANCIA = new JsfUtil();
	
	/**
	 * Obtiene la instancia
	 * 		@author sladino
	 * @return Instancia
	 */
	public JsfUtil getInstancia() {
		return INSTANCIA;
	}
	
	/**
	 * Permite obtener el contexto de la instancia actual del facesContext.
	 * 	@author sladino
	 * @return Contexto de la instancia actual
	 */
	public static FacesContext getCurrentInstanceFacesContext() {
		return FacesContext.getCurrentInstance();
	}

	/**
	 * Permite obtener el contexto de la instancia actual del request.
	 * 	@author sladino
	 * @return Contexto de la instancia actual del request
	 */
	public static RequestContext getCurrentInstanceRequestContext() {
		return RequestContext.getCurrentInstance();
	}

	/**
	 * Remueve un bean de session
	 * 		@author sladino
	 * @param pManagedBean	Bean a remover
	 */
	public static void removeManagedBean(String pManagedBean) {
		getCurrentInstanceFacesContext().getExternalContext().getSessionMap().remove(pManagedBean);
	}

	/**
	 * Cierra (Invalida) una sesion activa
	 * 		@author sladino
	 */
	public static void invalidateSession() {
		getCurrentInstanceFacesContext().getExternalContext().invalidateSession();
	}

	/**
	 * Aniade y muestra los mensajes de Exito, error o advertencia en las paginas web
	 * 		@author sladino
	 * @param pNivel	Tipo mensaje (Informativo, error, advertencia, etc)
	 * @param pMessage	Mensaje a mostrar
	 */
	public static void addMessage(Integer pNivel, String pMessage) {
		FacesMessage facesMessage = null;

		// Mensaje de error
		if (pNivel.equals(ConstantesUtil.MSJ_ERROR)) {
			facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, pMessage, null);
		}

		// Mensaje de error fatal
		if (pNivel.equals(ConstantesUtil.MSJ_FATAL)) {
			facesMessage = new FacesMessage(FacesMessage.SEVERITY_FATAL, pMessage, null);
		}

		// Mensaje de advertencia
		if (pNivel.equals(ConstantesUtil.MSJ_WARN)) {
			facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN, pMessage, null);
		}

		// Mensaje de informacion
		if (pNivel.equals(ConstantesUtil.MSJ_INFO)) {
			facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, pMessage, null);
		}

		getCurrentInstanceFacesContext().addMessage(null, facesMessage);
	}

	/**
	 * Permite obtener el objeto request de la instancia actual
	 * 	@author sladino
	 * @return Objeto Request
	 */
	public static HttpServletRequest getHttpServletRequest() {
		HttpServletRequest httpServletRequest = (HttpServletRequest) getCurrentInstanceFacesContext()
				.getExternalContext().getRequest();

		return httpServletRequest;
	}

	/**
	 * Obtiene el path del contexto de la aplicacion
	 * 		@author sladino
	 * @return	Path del contexto de la aplicacion
	 */
	public static String getContextPath() {
		String contextPath = getHttpServletRequest().getContextPath();

		return contextPath;
	}

	/**
	 * Obtiene el contexto del servlet
	 * 		@author sladino
	 * @return contexto del servlet
	 */
	public static ServletContext getServletContext() {
		ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();

		return servletContext;
	}

	/**
	 * Obtiene el path real de la aplicacion
	 * 		@author sladino
	 * @return	path real de la aplicacion
	 */
//	public static String getRealPath() {
//		String realPath = getServletContext().getRealPath(ConstantesUtil.SLASH);
//
//		return realPath;
//	}

	/**
	 * Obtiene el path real de la aplicacion de un directorio especifico
	 * 		@author sladino
	 * @param pDirectorio	Directorio cuyo path se desea obtener
	 * @return	path real de la aplicacion de un directorio especifico
	 */
	public static String getRealPath(String pDirectorio) {
		String realPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("//" + pDirectorio);

		return realPath;
	}

	/**
	 * Obtiene la Url
	 * 		@author sladino
	 * @return Url
	 */
	public static StringBuffer getRequestURL() {
		StringBuffer requestURL = ((HttpServletRequest) getCurrentInstanceFacesContext()
				.getExternalContext().getRequest()).getRequestURL();

		return requestURL;
	}

//	/**
//	 * Permite descargar el tipo de comprobante
//	 *  	@author sladino
//	 *  @return	Path 
//	 */
//	public static String pathImageApplication() {
//		String pathImageApplication = JsfUtil.getRealPath() + ConstantesUtil.DIRECTORIO_SERVER_RESOURCES 
//	+ ConstantesUtil.BACK_SLASH + ConstantesUtil.DIRECTORIO_SERVER_IMAGES + ConstantesUtil.BACK_SLASH;
//
//		return pathImageApplication;
//	}

	/**
	 * Permite descargar el reporte
	 * 
	 * @param pTipoComprobanteDescargar	Tipo comprobante a descargar (pdf, xml, etc)
	 * @throws IOException	En caso de error
	 */
//	public static void downloadFile(String pTipoComprobanteDescargar) throws IOException {
//		getCurrentInstanceFacesContext().getExternalContext().redirect(getCurrentInstanceFacesContext()
//				.getExternalContext().getRequestContextPath() + ConstantesUtil.DESCARGADOR_SERVLET 
//				+ pTipoComprobanteDescargar);
//		getCurrentInstanceFacesContext().responseComplete();
//	}

//	/**
//	 * Permite obtener el contexto del path de la imagen para generar el codigo de barras.
//	 * 
//	 * @param IOException
//	 */
//	public static String getContextPathImagen() {
//		String[] contextPath = getRequestURL().toString().split(getContextPath());
//		String contextPathImagen = contextPath[0].toString() + getContextPath();
//
//		return contextPathImagen;
//	}

	/**
	 * Permite llamar al metodo CallBackParam.
	 * @param pParametro	Parametro
	 */
//	public static void addCallbackParam(Boolean pParametro) {
//		getCurrentInstanceRequestContext().addCallbackParam(ConstantesUtil.PARAM_ACEPTABLE, 
//				Boolean.valueOf(pParametro));
//	}

	/**
	 * Obtiene valor de un parametro dentro de la peticion de request
	 * 		@author sladino
	 * @param pParametro	Parametro
	 * @return	Valor del parametro
	 */
	public static String getParameter(String pParametro) {
		HttpServletRequest httpServletRequest = (HttpServletRequest) getCurrentInstanceFacesContext()
				.getExternalContext().getRequest();
		
		return httpServletRequest.getParameter(pParametro);
	}
}
