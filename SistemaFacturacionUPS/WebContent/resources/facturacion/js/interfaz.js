$(document).ready(function() {
	$(".submit_login").hover(function(event){
		$(".submit_login").addClass("submit_over");
	},function(event){
		$(".submit_login").removeClass("submit_over");
	});
	
	
	
	$(".option_menu").click(function(event){
		
		if($('div.menu').hasClass("big")==true){
			$("div.menu").removeClass("big");
			$("div.menu").addClass("short");
			$("body").removeClass("big");
			$("body").addClass("short");
			$(".option_menu").removeClass("big");
			$(".option_menu").addClass("short");
			
			$(".panel").removeClass("big");
			$(".panel").addClass("short");
				
		}else{
			$("div.menu").removeClass("short");
			$("div.menu").addClass("big");
			$("body").removeClass("short");
			$("body").addClass("big");	
			
			$(".option_menu").removeClass("short");
			$(".option_menu").addClass("big");
			
			$(".panel").removeClass("short");
			$(".panel").addClass("big");
			
		}
		
	});
	
	
	$(".option_submenu").click(function(event){
		if($('.panel .inside .box .submenu').hasClass("big")==true){
			$(".panel .inside .box .submenu").removeClass("big");
			$(".panel .inside .box .submenu").addClass("short");
			
			$(".panel .inside .box").removeClass("big");
			$(".panel .inside .box").addClass("short");
			$(".panel .inside .box .content").css("padding-right","35px");
			
	
		}else{
			$(".panel .inside .box .submenu").removeClass("short");
			$(".panel .inside .box .submenu").addClass("big");

			$(".panel .inside .box").removeClass("short");
			$(".panel .inside .box").addClass("big");
			
			$(".panel .inside .box .content").css("padding-right","102px");
			
		}
	});
	
	
	$(".display_columna_izq").click(function(event){
		
		if ($(".panel .inside .box  div.ui-layout-west").is(":hidden")) {
			$(".panel .inside .box  div.ui-layout-west").slideDown("slow");
			
			$(".panel .inside .box  div.ui-layout-center").css("width","75%");
		
		} else {
			
			$(".panel .inside .box  div.ui-layout-center").css("width","100%");
			$(".panel .inside .box  div.ui-layout-west").hide();
			
		}
		
	});
	
	
	$(".boton").click(function(event){
		id=this.id;
		id=id.split("-");
		id=id[1];
		
		if ($("#submenu-"+id).is(":hidden")) {
			$("#submenu-"+id).slideDown("slow");
		} else {
			$("#submenu-"+id).hide();
		}
		
		
	});
	
});

$(document).ready(function(){

	if(!Modernizr.input.placeholder){

		$('[placeholder]').focus(function() {
		  var input = $(this);
		  if (input.val() == input.attr('placeholder')) {
			input.val('');
			input.removeClass('placeholder');
		  }
		}).blur(function() {
		  var input = $(this);
		  if (input.val() == '' || input.val() == input.attr('placeholder')) {
			input.addClass('placeholder');
			input.val(input.attr('placeholder'));
		  }
		}).blur();
		$('[placeholder]').parents('form').submit(function() {
		  $(this).find('[placeholder]').each(function() {
			var input = $(this);
			if (input.val() == input.attr('placeholder')) {
			  input.val('');
			}
		  })
		});

	}

	});


function formatNumberText(objetoValor){
	if(isNaN(parseInt(objetoValor.value))){
		return;
	}
	objetoValor.value = formatNumber(parseFloat(objetoValor.value));
}