package com.ec.ups.service.estructura;

import java.util.Collection;

public class responseEstructure {

	private int status;
	
	private String message;

	private Collection<?> data;

	private String singleData;


	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Collection<?> getData() {
		return data;
	}

	public void setData(Collection<?> data) {
		this.data = data;
	}

	public String getSingleData() {
		return singleData;
	}

	public void setSingleData(String singleData) {
		this.singleData = singleData;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}


}
