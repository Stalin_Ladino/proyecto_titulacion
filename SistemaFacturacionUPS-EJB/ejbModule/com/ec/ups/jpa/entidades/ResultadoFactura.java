package com.ec.ups.jpa.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * The persistent class for the tbl_facturas database table.
 * 
 */
@Entity
@Table(name = "ResultadoFactura")
public class ResultadoFactura implements Serializable {
	private static final long serialVersionUID = 1L;

	private Date fecha_reg;
	private Long tot_consum;
	@Id
	private Integer id_consum;
	private String id_cod_med;
	private Boolean estado_facturado;
	private String modelo;
	private Integer id_med;
	private String ced_usu;
	private String dir_usu;
	private String nom_usu;
	private String tel_usu;
	private Integer id_usu;
	
	private String nom_sec;
	private Integer id_sec;
	private String nom_serv;
	private Integer id_serv;
	
	
	public Date getFecha_reg() throws Exception {
		return fecha_reg;
	}
	public void setFecha_reg(Date fecha_reg) {
		this.fecha_reg = fecha_reg;
	}
	public Long getTot_consum() {
		return tot_consum;
	}
	public void setTot_consum(Long tot_consum) {
		this.tot_consum = tot_consum;
	}
	public Integer getId_consum() {
		return id_consum;
	}
	public void setId_consum(Integer id_consum) {
		this.id_consum = id_consum;
	}
	public String getId_cod_med() {
		return id_cod_med;
	}
	public void setId_cod_med(String id_cod_med) {
		this.id_cod_med = id_cod_med;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public Integer getId_med() {
		return id_med;
	}
	public void setId_med(Integer id_med) {
		this.id_med = id_med;
	}
	public String getCed_usu() {
		return ced_usu;
	}
	public void setCed_usu(String ced_usu) {
		this.ced_usu = ced_usu;
	}
	public String getDir_usu() {
		return dir_usu;
	}
	public void setDir_usu(String dir_usu) {
		this.dir_usu = dir_usu;
	}
	public String getNom_usu() {
		return nom_usu;
	}
	public void setNom_usu(String nom_usu) {
		this.nom_usu = nom_usu;
	}
	public String getTel_usu() {
		return tel_usu;
	}
	public void setTel_usu(String tel_usu) {
		this.tel_usu = tel_usu;
	}
	public Integer getId_usu() {
		return id_usu;
	}
	public void setId_usu(Integer id_usu) {
		this.id_usu = id_usu;
	}
	public String getNom_sec() {
		return nom_sec;
	}
	public void setNom_sec(String nom_sec) {
		this.nom_sec = nom_sec;
	}
	public Integer getId_sec() {
		return id_sec;
	}
	public void setId_sec(Integer id_sec) {
		this.id_sec = id_sec;
	}
	public String getNom_serv() {
		return nom_serv;
	}
	public void setNom_serv(String nom_serv) {
		this.nom_serv = nom_serv;
	}
	public Integer getId_serv() {
		return id_serv;
	}
	public void setId_serv(Integer id_serv) {
		this.id_serv = id_serv;
	}
	public Boolean getEstado_facturado() {
		return estado_facturado;
	}
	public void setEstado_facturado(Boolean estado_facturado) {
		this.estado_facturado = estado_facturado;
	}
	
}