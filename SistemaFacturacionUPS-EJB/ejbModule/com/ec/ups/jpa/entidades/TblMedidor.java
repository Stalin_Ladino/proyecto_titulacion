package com.ec.ups.jpa.entidades;

import java.io.Serializable;

import javax.persistence.*;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the tbl_medidor database table.
 * 
 */
@Entity
@Table(name="tbl_medidor")
@NamedQuery(name="TblMedidor.findAll", query="SELECT t FROM TblMedidor t")
public class TblMedidor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_med")
	private Integer idMed;

	@Column(name="id_cod_med")
	private String idCod;

	@Column(name="cont_ini_med")
	private BigDecimal contIniMed;

	@Temporal(TemporalType.DATE)
	@Column(name="fecha_ing_med")
	private Date fechaIngMed;

	private String modelo;

	@Column(name="tiem_vid_med")
	private String tiemVidMed;
	
	@Column(name="id_reg")
	private Integer id_reg;

	@Column(name="id_sec")
	private Integer id_sec;
	
	@Column(name="id_serv")
	private Integer id_serv;
	
	@Column(name="id_usu")
	private Integer id_usu;
	
	//bi-directional many-to-one association to TblRegional
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="id_reg",updatable=false,insertable=false)
	private TblRegional tblRegional;

	//bi-directional many-to-one association to TblSecCom
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="id_sec",updatable=false,insertable=false)
	private secComDTO tblSecCom;

	//bi-directional many-to-one association to TblServicio
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="id_serv", updatable=false,insertable=false)
	private TblServicio tblServicio;

	//bi-directional many-to-one association to TblUsuario

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="id_usu",updatable=false,insertable=false)
	private usuarioDTO tblUsuario;

	public TblMedidor() {
	}

	public Integer getIdMed() {
		return this.idMed;
	}

	public void setIdMed(Integer idMed) {
		this.idMed = idMed;
	}

	public BigDecimal getContIniMed() {
		return this.contIniMed;
	}

	public void setContIniMed(BigDecimal contIniMed) {
		this.contIniMed = contIniMed;
	}

	public Date getFechaIngMed() {
		return this.fechaIngMed;
	}

	public void setFechaIngMed(Date fechaIngMed) {
		this.fechaIngMed = fechaIngMed;
	}

	public String getModelo() {
		return this.modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getTiemVidMed() {
		return this.tiemVidMed;
	}

	public void setTiemVidMed(String tiemVidMed) {
		this.tiemVidMed = tiemVidMed;
	}

	public TblRegional getTblRegional() {
		return this.tblRegional;
	}

	public void setTblRegional(TblRegional tblRegional) {
		this.tblRegional = tblRegional;
	}

	public secComDTO getTblSecCom() {
		return this.tblSecCom;
	}

	public void setTblSecCom(secComDTO tblSecCom) {
		this.tblSecCom = tblSecCom;
	}

	public TblServicio getTblServicio() {
		return this.tblServicio;
	}

	public void setTblServicio(TblServicio tblServicio) {
		this.tblServicio = tblServicio;
	}

	public usuarioDTO getTblUsuario() {
		return this.tblUsuario;
	}

	public void setTblUsuario(usuarioDTO tblUsuario) {
		this.tblUsuario = tblUsuario;
	}

	public Integer getId_reg() {
		return id_reg;
	}

	public void setId_reg(Integer id_reg) {
		this.id_reg = id_reg;
	}

	public Integer getId_sec() {
		return id_sec;
	}

	public void setId_sec(Integer id_sec) {
		this.id_sec = id_sec;
	}

	public Integer getId_serv() {
		return id_serv;
	}

	public void setId_serv(Integer id_serv) {
		this.id_serv = id_serv;
	}

	public Integer getId_usu() {
		return id_usu;
	}

	public void setId_usu(Integer id_usu) {
		this.id_usu = id_usu;
	}

	public String getIdCod() {
		return idCod;
	}

	public void setIdCod(String idCod) {
		this.idCod = idCod;
	}

}