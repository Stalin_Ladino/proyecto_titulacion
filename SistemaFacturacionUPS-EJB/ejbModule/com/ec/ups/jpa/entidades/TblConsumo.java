package com.ec.ups.jpa.entidades;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the tbl_consumo database table.
 * 
 */
@Entity
@Table(name="tbl_consumo")
@NamedQuery(name="TblConsumo.findAll", query="SELECT t FROM TblConsumo t")
public class TblConsumo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_consum")
	private Integer idConsum;

	@Temporal(TemporalType.DATE)
	@Column(name="fecha_reg")
	private Date fechaReg;

	@Column(name="id_cod_med")
	private String idCodMed;

	@Column(name="id_med")
	private Integer idMed;

	@Column(name="id_sec")
	private Integer idSec;

	@Column(name="tot_consum")
	private Double totConsum;
	
	@Column(name="estado_facturado")
	private Boolean estadoFacturado;
	
	@Transient
	private String nomUser;
	
	@Transient
	private String cedUser;
	
	@Transient
	private String fechaConsumo;
	
	public TblConsumo() {
	}

	public Integer getIdConsum() {
		return this.idConsum;
	}

	public void setIdConsum(Integer idConsum) {
		this.idConsum = idConsum;
	}

	public Date getFechaReg() {
		return this.fechaReg;
	}

	public void setFechaReg(Date fechaReg) {
		this.fechaReg = fechaReg;
	}

	public String getIdCodMed() {
		return this.idCodMed;
	}

	public void setIdCodMed(String idCodMed) {
		this.idCodMed = idCodMed;
	}

	public Integer getIdMed() {
		return this.idMed;
	}

	public void setIdMed(Integer idMed) {
		this.idMed = idMed;
	}

	public Integer getIdSec() {
		return this.idSec;
	}

	public void setIdSec(Integer idSec) {
		this.idSec = idSec;
	}

	public Double getTotConsum() {
		return this.totConsum;
	}

	public void setTotConsum(Double totConsum) {
		this.totConsum = totConsum;
	}

	public String getNomUser() {
		return nomUser;
	}

	public void setNomUser(String nomUser) {
		this.nomUser = nomUser;
	}

	public String getCedUser() {
		return cedUser;
	}

	public void setCedUser(String cedUser) {
		this.cedUser = cedUser;
	}

	public String getFechaConsumo() {
		return fechaConsumo;
	}

	public void setFechaConsumo(String fechaConsumo) {
		this.fechaConsumo = fechaConsumo;
	}

	public Boolean getEstadoFacturado() {
		return estadoFacturado;
	}

	public void setEstadoFacturado(Boolean estadoFacturado) {
		this.estadoFacturado = estadoFacturado;
	}
}