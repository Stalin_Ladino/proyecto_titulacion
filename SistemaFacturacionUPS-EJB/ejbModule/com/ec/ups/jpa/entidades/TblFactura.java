package com.ec.ups.jpa.entidades;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the tbl_facturas database table.
 * 
 */
@Entity
@Table(name="tbl_facturas")
@NamedQuery(name="TblFactura.findAll", query="SELECT t FROM TblFactura t")
public class TblFactura implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_fact")
	private Integer idFact;

	@Column(name="cedula_usu")
	private String cedulaUsu;

	@Column(name="consumo_m3")
	private Long consumoM3;

	@Temporal(TemporalType.DATE)
	@Column(name="fech_fact")
	private Date fechFact;

	@Column(name="id_med_fact")
	private Integer idMedFact;

	@Column(name="id_sect_fact")
	private Integer idSectFact;

	@Column(name="id_usu_fact")
	private Integer idUsuFact;

	@Column(name="tot_consumo")
	private Double totConsumo;

	@Column(name="tot_sanciones")
	private Double totSanciones;

	@Column(name="total_pagar")
	private Double totalPagar;

	public TblFactura() {
	}

	public Integer getIdFact() {
		return this.idFact;
	}

	public void setIdFact(Integer idFact) {
		this.idFact = idFact;
	}

	public String getCedulaUsu() {
		return this.cedulaUsu;
	}

	public void setCedulaUsu(String cedulaUsu) {
		this.cedulaUsu = cedulaUsu;
	}

	public Long getConsumoM3() {
		return this.consumoM3;
	}

	public void setConsumoM3(Long consumoM3) {
		this.consumoM3 = consumoM3;
	}

	public Date getFechFact() {
		return this.fechFact;
	}

	public void setFechFact(Date fechFact) {
		this.fechFact = fechFact;
	}

	public Integer getIdMedFact() {
		return this.idMedFact;
	}

	public void setIdMedFact(Integer idMedFact) {
		this.idMedFact = idMedFact;
	}

	public Integer getIdSectFact() {
		return this.idSectFact;
	}

	public void setIdSectFact(Integer idSectFact) {
		this.idSectFact = idSectFact;
	}

	public Integer getIdUsuFact() {
		return this.idUsuFact;
	}

	public void setIdUsuFact(Integer idUsuFact) {
		this.idUsuFact = idUsuFact;
	}

	public Double getTotConsumo() {
		return this.totConsumo;
	}

	public void setTotConsumo(Double totConsumo) {
		this.totConsumo = totConsumo;
	}

	public Double getTotSanciones() {
		return this.totSanciones;
	}

	public void setTotSanciones(Double totSanciones) {
		this.totSanciones = totSanciones;
	}

	public Double getTotalPagar() {
		return this.totalPagar;
	}

	public void setTotalPagar(Double totalPagar) {
		this.totalPagar = totalPagar;
	}

}