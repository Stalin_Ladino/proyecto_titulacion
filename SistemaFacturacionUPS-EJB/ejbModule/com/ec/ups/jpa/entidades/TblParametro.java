package com.ec.ups.jpa.entidades;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the tbl_parametros database table.
 * 
 */
@Entity
@Table(name="tbl_parametros")
@NamedQuery(name="TblParametro.findAll", query="SELECT t FROM TblParametro t")
public class TblParametro implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_param")
	private Integer idParam;

	@Column(name="id_sec")
	private Integer idSec;
//
//	@Column(name="meses_dif")
//	private Integer mesesDif;

	@Column(name="reg_max")
	private Double regMax;

	@Column(name="reg_min")
	private Double regMin;

	@Column(name="val_instal")
	private Double valInstal;

	@Column(name="val_mcub")
	private Double valMcub;

	@Column(name="val_multa")
	private Double valMulta;
	
	@Column(name="descripcion")
	private String descripcion;
	
	@Column(name="id_serv")
	private Integer idServ;
	
//	@Column(name="num_mcub")
//	private Integer numMcub;
	
//	@Column(name="val_exced")
//	private Integer valExced;
	
	@ManyToOne
	@JoinColumn(name="id_sec",insertable=false,updatable=false)
	private secComDTO secComDTO;
	
	@ManyToOne
	@JoinColumn(name="id_serv",insertable=false,updatable=false)
	private TblServicio tblServicio;
	
	//bi-directional many-to-one association to TblMedidor
	@OneToMany(mappedBy="tblParametro",orphanRemoval=true)
	private List<TblExcedente1> tblExcedente;

	public TblParametro() {
	}

	public Integer getIdParam() {
		return this.idParam;
	}

	public void setIdParam(Integer idParam) {
		this.idParam = idParam;
	}

	public Integer getIdSec() {
		return this.idSec;
	}

	public void setIdSec(Integer idSec) {
		this.idSec = idSec;
	}

//	public Integer getMesesDif() {
//		return this.mesesDif;
//	}
//
//	public void setMesesDif(Integer mesesDif) {
//		this.mesesDif = mesesDif;
//	}

	public Double getRegMax() {
		return this.regMax;
	}

	public void setRegMax(Double regMax) {
		this.regMax = regMax;
	}

	public Double getRegMin() {
		return this.regMin;
	}

	public void setRegMin(Double regMin) {
		this.regMin = regMin;
	}

	public Double getValInstal() {
		return this.valInstal;
	}

	public void setValInstal(Double valInstal) {
		this.valInstal = valInstal;
	}

	public Double getValMcub() {
		return this.valMcub;
	}

	public void setValMcub(Double valMcub) {
		this.valMcub = valMcub;
	}

	public Double getValMulta() {
		return this.valMulta;
	}

	public void setValMulta(Double valMulta) {
		this.valMulta = valMulta;
	}

	public secComDTO getSecComDTO() {
		return secComDTO;
	}

	public void setSecComDTO(secComDTO secComDTO) {
		this.secComDTO = secComDTO;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getIdServ() {
		return idServ;
	}

	public void setIdServ(Integer idServ) {
		this.idServ = idServ;
	}

	public TblServicio getTblServicio() {
		return tblServicio;
	}

	public void setTblServicio(TblServicio tblServicio) {
		this.tblServicio = tblServicio;
	}
//
//	public Integer getNumMcub() {
//		return numMcub;
//	}
//
//	public void setNumMcub(Integer numMcub) {
//		this.numMcub = numMcub;
//	}

//	public Integer getValExced() {
//		return valExced;
//	}
//
//	public void setValExced(Integer valExced) {
//		this.valExced = valExced;
//	}


}