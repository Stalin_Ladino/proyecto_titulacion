package com.ec.ups.jpa.entidades;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the tbl_excedente database table.
 * 
 */
@Entity
@Table(name="tbl_excedente")
@NamedQuery(name="TblExcedente1.findAll", query="SELECT t FROM TblExcedente1 t")
public class TblExcedente1 implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_exc")
	private Integer idExc;

	@Column(name="id_param")
	private Integer idParam;

//	@Column(name="num_mcub")
//	private Integer numMcub;
//
//	@Column(name="val_mcub")
//	private Double valMcub;

	@Transient
	private Integer numMcubT;

	@Transient
	private Double valMcubT;

	@ManyToOne
	@JoinColumn(name="id_param", updatable=false,insertable=false)
	private TblParametro tblParametro;
	
	public TblExcedente1() {
	}

	public Integer getIdExc() {
		return this.idExc;
	}

	public void setIdExc(Integer idExc) {
		this.idExc = idExc;
	}

	public Integer getIdParam() {
		return this.idParam;
	}

	public void setIdParam(Integer idParam) {
		this.idParam = idParam;
	}

//	public Integer getNumMcub() {
//		return this.numMcub;
//	}
//
//	public void setNumMcub(Integer numMcub) {
//		this.numMcub = numMcub;
//	}
//
//	public Double getValMcub() {
//		return this.valMcub;
//	}
//
//	public void setValMcub(Double valMcub) {
//		this.valMcub = valMcub;
//	}

	public Integer getNumMcubT() {
		return numMcubT;
	}

	public void setNumMcubT(Integer numMcubT) {
		this.numMcubT = numMcubT;
	}

	public Double getValMcubT() {
		return valMcubT;
	}

	public void setValMcubT(Double valMcubT) {
		this.valMcubT = valMcubT;
	}

	
}