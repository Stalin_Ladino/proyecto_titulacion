package com.ec.ups.jpa.entidades;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the tbl_excedente database table.
 * 
 */
@Entity
@Table(name="tbl_excedente")
@NamedQuery(name="TblExcedente.findAll", query="SELECT t FROM TblExcedente t")
public class TblExcedente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_exc")
	private Integer idExc;

	@Column(name="id_param")
	private Integer idParam;

	@Column(name="reg_exc_max")
	private double regExcMax;

	@Column(name="reg_exc_min")
	private double regExcMin;

	@Column(name="val_exc_mcub")
	private double valExcMcub;

	public TblExcedente() {
	}

	public Integer getIdExc() {
		return this.idExc;
	}

	public void setIdExc(Integer idExc) {
		this.idExc = idExc;
	}

	public Integer getIdParam() {
		return this.idParam;
	}

	public void setIdParam(Integer idParam) {
		this.idParam = idParam;
	}

	public double getRegExcMax() {
		return this.regExcMax;
	}

	public void setRegExcMax(double regExcMax) {
		this.regExcMax = regExcMax;
	}

	public double getRegExcMin() {
		return this.regExcMin;
	}

	public void setRegExcMin(double regExcMin) {
		this.regExcMin = regExcMin;
	}

	public double getValExcMcub() {
		return this.valExcMcub;
	}

	public void setValExcMcub(double valExcMcub) {
		this.valExcMcub = valExcMcub;
	}

}