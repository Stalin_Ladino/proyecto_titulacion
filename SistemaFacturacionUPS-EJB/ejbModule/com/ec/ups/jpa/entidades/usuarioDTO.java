package com.ec.ups.jpa.entidades;

import java.io.Serializable;

import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the tbl_usuario database table.
 * 
 */
@Entity
@Table(name="tbl_usuario")
@NamedQuery(name="usuarioDTO.findAll", query="SELECT t FROM usuarioDTO t")
public class usuarioDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_usu")
	private Integer idUsu;

	@Column(name="ced_usu")
	private String cedUsu;

	@Column(name="cel_usu")
	private String celUsu;

	@Column(name="dir_usu")
	private String dirUsu;

	@Column(name="etn_usu")
	private String etnUsu;

	@Column(name="nom_usu")
	private String nomUsu;

	@Column(name="num_fam")
	private Integer numFam;

	@Column(name="tel_usu")
	private String telUsu;
	
	
	//RELACIONAR BIEN LA NUEVA TABLA SECTOR POR USUAARIO
//	@Column(name="id_sec")
//	private Integer idSec;
	
	@Column(name="id_login")
	private Integer idLogin;

	//bi-directional many-to-one association to TblMedidor
	@OneToMany(mappedBy="tblUsuario")
	private List<TblMedidor> tblMedidors;

	//bi-directional many-to-one association to TblLogin
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="id_login", insertable=false ,updatable=false)
	private loginDTO loginDTO;

	//bi-directional many-to-one association to TblSecCom
//	@ManyToOne(cascade = CascadeType.ALL)
//	@JoinColumn(name="id_sec", insertable=false ,updatable=false)
//	private secComDTO secComDTO;
	
	//bi-directional many-to-one association to TblMedidor
	@OneToMany(mappedBy="tblUsuario")
	private List<TblUsuarioMulta> tblUsuarioMulta;
	
	// bi-directional many-to-one association to TblMedidor
	@OneToMany(mappedBy = "tblUsuario")
	private List<TblSectorUsuario> tblSectorUsuario;

	public usuarioDTO() {
	}

	public Integer getIdUsu() {
		return this.idUsu;
	}

	public void setIdUsu(Integer idUsu) {
		this.idUsu = idUsu;
	}

	public String getCedUsu() {
		return this.cedUsu;
	}

	public void setCedUsu(String cedUsu) {
		this.cedUsu = cedUsu;
	}

	public String getCelUsu() {
		return this.celUsu;
	}

	public void setCelUsu(String celUsu) {
		this.celUsu = celUsu;
	}

	public String getDirUsu() {
		return this.dirUsu;
	}

	public void setDirUsu(String dirUsu) {
		this.dirUsu = dirUsu;
	}

	public String getEtnUsu() {
		return this.etnUsu;
	}

	public void setEtnUsu(String etnUsu) {
		this.etnUsu = etnUsu;
	}

	public String getNomUsu() {
		return this.nomUsu;
	}

	public void setNomUsu(String nomUsu) {
		this.nomUsu = nomUsu;
	}

	public Integer getNumFam() {
		return this.numFam;
	}

	public void setNumFam(Integer numFam) {
		this.numFam = numFam;
	}

	public String getTelUsu() {
		return this.telUsu;
	}

	public void setTelUsu(String telUsu) {
		this.telUsu = telUsu;
	}

	public List<TblMedidor> getTblMedidors() {
		return this.tblMedidors;
	}

	public void setTblMedidors(List<TblMedidor> tblMedidors) {
		this.tblMedidors = tblMedidors;
	}

	public TblMedidor addTblMedidor(TblMedidor tblMedidor) {
		getTblMedidors().add(tblMedidor);
		tblMedidor.setTblUsuario(this);

		return tblMedidor;
	}

	public TblMedidor removeTblMedidor(TblMedidor tblMedidor) {
		getTblMedidors().remove(tblMedidor);
		tblMedidor.setTblUsuario(null);

		return tblMedidor;
	}

	
	public loginDTO getLoginDTO() {
		return this.loginDTO;
	}

	public void setLoginDTO(loginDTO loginDTO) {
		this.loginDTO = loginDTO;
	}

//	public secComDTO getSecComDTO() {
//		return this.secComDTO;
//	}
//
//	public void setSecComDTO(secComDTO secComDTO) {
//		this.secComDTO = secComDTO;
//	}

	

	public Integer getIdLogin() {
		return idLogin;
	}

	public List<TblSectorUsuario> getTblSectorUsuario() {
		return tblSectorUsuario;
	}

	public void setTblSectorUsuario(List<TblSectorUsuario> tblSectorUsuario) {
		this.tblSectorUsuario = tblSectorUsuario;
	}

	public void setIdLogin(Integer idLogin) {
		this.idLogin = idLogin;
	}

	public List<TblUsuarioMulta> getTblUsuarioMulta() {
		return tblUsuarioMulta;
	}

	public void setTblUsuarioMulta(List<TblUsuarioMulta> tblUsuarioMulta) {
		this.tblUsuarioMulta = tblUsuarioMulta;
	}

}