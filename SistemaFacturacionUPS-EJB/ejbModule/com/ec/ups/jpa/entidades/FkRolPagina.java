package com.ec.ups.jpa.entidades;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the fk_rol_paginas database table.
 * 
 */
@Entity
@Table(name="fk_rol_paginas")
@NamedQuery(name="FkRolPagina.findAll", query="SELECT f FROM FkRolPagina f")
public class FkRolPagina implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private FkRolPaginaPK id;

	public FkRolPagina() {
	}

	public FkRolPaginaPK getId() {
		return this.id;
	}

	public void setId(FkRolPaginaPK id) {
		this.id = id;
	}

}