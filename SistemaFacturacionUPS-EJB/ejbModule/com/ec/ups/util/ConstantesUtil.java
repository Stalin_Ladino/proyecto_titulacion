package com.ec.ups.util;

/**
 * Clase utilitaria para constantes
 *
 * Creado: 16/09/2016
 * 
 * @author sladino
 *
 */
public class ConstantesUtil {

	// Mensajes
	public static final Long NUMERO_CERO= 0L;
	public static final Long NUMERO_UNO= 1L;
	public static final Long NUMERO_DOS= 2L;
	public static final Long NUMERO_TRES= 3L;
	public static final Long NUMERO_CUATRO= 4L;
	
	public static final Integer NUMERO_CERO_INT= 0;
	public static final Integer NUMERO_UNO_INT= 1;
	public static final Integer NUMERO_DOS_INT= 2;
	public static final Integer NUMERO_TRES_INT= 3;
	public static final Integer NUMERO_CUATRO_INT= 4;
	
	public final static Integer MSJ_ERROR = 1;
	public final static Integer MSJ_FATAL = 2;
	public final static Integer MSJ_WARN = 3;
	public final static Integer MSJ_INFO = 4;
	
	public final static Integer NIVEL_SUPER_ADMI = 1;
	public final static Integer NIVEL_ADMINISTRADOR = 2;
	
	public final static Integer MAX_ITEMS_TABLA=10;
	
	public static final String CADENA_VACIA= "";
	public static final String PANTALLA_CONSULTAR= "C";
	public static final String PANTALLA_MODIFICAR= "M";
	public static final String PANTALLA_NUEVO= "N";
	
	public static final String ACCION_MODIFICAR= "M";
	public static final String ACCION_NUEVO= "N";
	public static final String ACCION_ELIMINAR= "E";
	public static final String ACCION_CONSULTAR= "C";
	public static final String ACCION_SIN_DATOS= "CV";
	
	public final static String REDIRIGIR_INICIO = "/page/index2?faces-redirect=true";
	public final static String PUNTO_Y_COMA = ";";
	
}
