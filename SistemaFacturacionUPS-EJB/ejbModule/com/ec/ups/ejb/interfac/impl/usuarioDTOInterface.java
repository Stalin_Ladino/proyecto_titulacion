package com.ec.ups.ejb.interfac.impl;

import java.util.List;

import javax.ejb.Local;

import com.ec.ups.jpa.entidades.TblSectorUsuario;
import com.ec.ups.jpa.entidades.usuarioDTO;

@Local
public interface usuarioDTOInterface {
	
	void create(usuarioDTO UsuarioDTO);

	void edit(usuarioDTO UsuarioDTO);

	void remove(usuarioDTO UsuarioDTO);
	
	void eliminarPorSector (Integer codsec);

	public usuarioDTO findById(Integer id);
	
	List<usuarioDTO> findAll();
	
	List<usuarioDTO> findUserAll();
	
	usuarioDTO findByIdLogin(Integer id);
	
	usuarioDTO findByCedula(String id);
	
	public List<usuarioDTO> buscarByCedula(String cedula);
	
	public List<usuarioDTO> buscarBySector(Integer idSector);
	
	public List<usuarioDTO> buscarByLoginList(Integer idLogin);
	
	public List<usuarioDTO> buscarByCedulaSector(String cedula,Integer idSector);
	
	public List<TblSectorUsuario> buscarByLoginSector(Integer idLogin);
}
