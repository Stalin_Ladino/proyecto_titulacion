package com.ec.ups.ejb.interfac.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import com.ec.ups.jpa.entidades.TblServicio;
import com.ec.ups.util.UtilConexion;

@Stateless
public class serviciosDTOImpl implements serviciosDTOInterface {

	@EJB
	private UtilConexion utilFacade;

	@Override
	public void create(TblServicio TblServicio) {
		utilFacade.getEm().persist(TblServicio);
	}

	@Override
	public void edit(TblServicio TblServicio) {
		utilFacade.getEm().merge(TblServicio);
		
	}

	@Override
	public void remove(TblServicio TblServicio) {
		utilFacade.getEm().remove(utilFacade.getEm().merge(TblServicio));
		
	}
	
//	
	public TblServicio findById(Integer id){
		TblServicio rol= new TblServicio();
		Query query=utilFacade.getEm().createQuery("Select rol from TblServicio as rol where idPer=:id ");
		query.setParameter("id", id);
		rol= (TblServicio) query.getSingleResult();
		return rol;
	}

	public List<TblServicio> findAll(){
		List<TblServicio> sect= new ArrayList<TblServicio>();
		Query query=utilFacade.getEm().createQuery("Select serv from TblServicio as serv");
		sect= query.getResultList();
		return sect;
	}
	

	
}
