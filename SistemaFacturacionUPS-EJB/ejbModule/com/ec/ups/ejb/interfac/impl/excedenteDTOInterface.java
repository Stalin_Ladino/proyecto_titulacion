package com.ec.ups.ejb.interfac.impl;

import java.util.List;

import javax.ejb.Local;

import com.ec.ups.jpa.entidades.TblExcedente;

@Local
public interface excedenteDTOInterface {
	
	void create(TblExcedente TblExcedente);

	void edit(TblExcedente TblExcedente);

	void remove(TblExcedente TblExcedente);

	public TblExcedente findById(Integer id);
	
	public List<TblExcedente> findAll();
	
	public List<TblExcedente> buscarIdParam(Integer IdParam);
	
	public TblExcedente findByIdLogin(Integer id);
	
}
