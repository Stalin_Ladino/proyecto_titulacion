package com.ec.ups.ejb.interfac.impl;

import java.util.List;

import javax.ejb.Local;

import com.ec.ups.jpa.entidades.TblMedidor;

@Local
public interface medidoresDTOInterface {
	
	void create(TblMedidor TblMedidor);

	void edit(TblMedidor TblMedidor);

	void remove(TblMedidor TblMedidor);

	public TblMedidor findById(Integer id);
	
	public List<TblMedidor> findAll();
	
	public List<TblMedidor> buscarPorIdUser(Integer id);
	
	public List<TblMedidor> buscarPorIdSector(Integer id,Integer idSector);
	
	public List<TblMedidor> findAllBySector(Integer idSector);
	
	public TblMedidor findMedidorParaConsumo(String codMedidor,Integer idSector);
	
	public TblMedidor findCodMedidor(String codMedidor);
	
}
