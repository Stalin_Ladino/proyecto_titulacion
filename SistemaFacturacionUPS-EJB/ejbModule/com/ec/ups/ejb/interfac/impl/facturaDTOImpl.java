package com.ec.ups.ejb.interfac.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.ec.ups.jpa.entidades.ResultadoFactura;
import com.ec.ups.jpa.entidades.TblFactura;
import com.ec.ups.util.UtilConexion;

@Stateless
public class facturaDTOImpl implements facturaDTOInterface {

	@EJB
	private UtilConexion utilFacade;

	@Override
	public void create(TblFactura TblFactura) {
		utilFacade.getEm().persist(TblFactura);
	}

	@Override
	public void edit(TblFactura TblFactura) {
		utilFacade.getEm().merge(TblFactura);
		
	}

	@Override
	public void remove(TblFactura TblFactura) {
		utilFacade.getEm().remove(utilFacade.getEm().merge(TblFactura));
		
	}
	
//	
	public List<ResultadoFactura> cargarFactura(String cedula,Integer idSector,
								Date fechaIni,Date fechaFin, Integer idServicio ){
		List<ResultadoFactura> resp= new ArrayList();
		

		SimpleDateFormat formatoDeFecha = new SimpleDateFormat("yyyy/MM/dd");
		String cedulaB=null;
		String fechaIniB=null;
		String fechaFinB=null;
		
		try{
			if(null!=cedula && !cedula.trim().equals("")){
				cedulaB="'"+cedula+"'";
			}
			if(null!=fechaIni){
				fechaIniB="'"+formatoDeFecha.format(fechaIni)+"'";
			}
			if(null!=fechaFin){
				fechaFinB="'"+formatoDeFecha.format(fechaFin)+"'";
			}
			
		String sql ="select"+
						" tbl_consumo.fecha_reg,tbl_consumo.tot_consum,tbl_consumo.id_consum,tbl_consumo.estado_facturado,"+
						" tbl_medidor.id_cod_med,tbl_medidor.modelo,tbl_medidor.id_med,"+
						" tbl_usuario.ced_usu,tbl_usuario.dir_usu,"+ 
						" tbl_usuario.nom_usu,tbl_usuario.tel_usu,tbl_usuario.id_usu,"+
						" tbl_sec_com.nom_sec,tbl_sec_com.id_sec,"+
						" tbl_servicio.nom_serv,tbl_servicio.id_serv"+ 
						
						" from tbl_consumo, tbl_medidor,tbl_usuario,tbl_sec_com,tbl_servicio,"+
						" tbl_sector_usuario"+
						" where tbl_consumo.id_med=tbl_medidor.id_med"+
						" and tbl_consumo.id_sec = tbl_sec_com.id_sec"+
						" and tbl_sector_usuario.id_sec= tbl_sec_com.id_sec"+
						" and tbl_sector_usuario.id_usu= tbl_usuario.id_usu"+
						" and tbl_medidor.id_sec= tbl_sec_com.id_sec"+
						" and tbl_medidor.id_usu= tbl_usuario.id_usu"+
						" and tbl_usuario.id_usu = tbl_medidor.id_usu"+
						" and tbl_servicio.id_serv=tbl_medidor.id_serv"+
						
						" and ("+cedulaB+ " IS NULL OR tbl_usuario.ced_usu ="+cedulaB+")"+
						" and ("+idSector+ " IS NULL OR tbl_sector_usuario.id_sec ="+idSector+")"+
						" and ("+fechaIniB+ " IS NULL AND "+fechaFinB+" IS NULL OR "
						+ "tbl_consumo.fecha_reg BETWEEN "+fechaIniB+" AND "+fechaFinB+")"+
						" and ("+idServicio+ " IS NULL OR tbl_medidor.id_serv="+idServicio+")";
		
		Query query = utilFacade.getEm().createNativeQuery(sql,ResultadoFactura.class);
		resp= query.getResultList();
		} catch(NoResultException e){
			resp=null;
		}
		return resp;
	}
}
