package com.ec.ups.ejb.interfac.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.ec.ups.jpa.entidades.TblMedidor;
import com.ec.ups.util.UtilConexion;

@Stateless
public class medidoresDTOImpl implements medidoresDTOInterface {

	@EJB
	private UtilConexion utilFacade;

	@Override
	public void create(TblMedidor TblMedidor) {
		TblMedidor.setTblSecCom(null);
		TblMedidor.setTblUsuario(null);
		utilFacade.getEm().persist(TblMedidor);
	}

	@Override
	public void edit(TblMedidor TblMedidor) {
		TblMedidor.setTblSecCom(null);
		TblMedidor.setTblUsuario(null);
		utilFacade.getEm().merge(TblMedidor);
		
	}

	@Override
	public void remove(TblMedidor TblMedidor) {
		TblMedidor.setTblRegional(null);
		TblMedidor.setTblServicio(null);
		TblMedidor.setTblSecCom(null);
		TblMedidor.setTblUsuario(null);
		utilFacade.getEm().remove(utilFacade.getEm().merge(TblMedidor));
		
	}
	
//	
	public TblMedidor findById(Integer id){
		TblMedidor rol= new TblMedidor();
		Query query=utilFacade.getEm().createQuery("Select rol from TblMedidor as rol where idPer=:id ");
		query.setParameter("id", id);
		rol= (TblMedidor) query.getSingleResult();
		return rol;
	}

	public List<TblMedidor> findAll(){
		List<TblMedidor> sect= new ArrayList<TblMedidor>();
		Query query=utilFacade.getEm().createQuery("Select med from TblMedidor as med, usuarioDTO as user,"
												 + " secComDTO as secCom,TblServicio as servi"
												 + " where med.id_usu=user.idUsu"
												 + " and secCom.idSec=med.id_sec"
												 + " and servi.idServ=med.id_serv"
												 + " ORDER BY med.id_usu ASC");
		sect= query.getResultList();
		return sect;
	}
	
	public List<TblMedidor> findAllBySector(Integer idSector){
		List<TblMedidor> sect= new ArrayList<TblMedidor>();
		Query query=utilFacade.getEm().createQuery("Select med from TblMedidor as med, usuarioDTO as user,"
												 + " secComDTO as secCom,TblServicio as servi"
												 + " where med.id_usu=user.idUsu"
												 + " and secCom.idSec=med.id_sec"
												 + " and servi.idServ=med.id_serv"
												 + " and med.id_sec=:idSector "
												 + " ORDER BY med.id_usu ASC");
		
		query.setParameter("idSector", idSector);
		sect= query.getResultList();
		return sect;
	}
	

	public List<TblMedidor> buscarPorIdUser(Integer id) {
		List<TblMedidor> rol= new ArrayList<TblMedidor>();
		try {
		Query query=utilFacade.getEm().createQuery("Select med from TblMedidor as med,usuarioDTO as user,"
												 + " secComDTO as secCom,TblServicio as servi"
												 + " where med.id_usu=user.idUsu"
												 + " and secCom.idSec=med.id_sec"
												 + " and med.id_usu=:id "
												 + " and servi.idServ=med.id_serv"
												 + " ORDER BY med.id_usu ASC");
		query.setParameter("id", id);
		rol= query.getResultList();
		} catch (NoResultException e){
			rol=new ArrayList<TblMedidor>();
		}
		return rol;
	}
	
	public List<TblMedidor> buscarPorIdSector(Integer id,Integer idSector) {
		List<TblMedidor> rol= new ArrayList<TblMedidor>();
		try {
		Query query=utilFacade.getEm().createQuery("Select med from TblMedidor as med,usuarioDTO as user,"
												 + " secComDTO as secCom,TblServicio as servi"
												 + " where med.id_usu=user.idUsu"
												 + " and secCom.idSec=med.id_sec"
												 + " and med.id_usu=:id "
												 + " and med.id_sec=:idSector "
												 + " and servi.idServ=med.id_serv"
												 + " ORDER BY med.id_usu ASC");
		query.setParameter("id", id);
		query.setParameter("idSector", idSector);
		rol= query.getResultList();
		} catch (NoResultException e){
			rol=new ArrayList<TblMedidor>();
		}
		return rol;
	}
	
	public TblMedidor findMedidorParaConsumo(String codMedidor,Integer idSector){
		TblMedidor medi= new TblMedidor();
		try {
		Query query=utilFacade.getEm().createQuery("Select med from TblMedidor as med, usuarioDTO as user,"
												 + " secComDTO as secCom,TblServicio as servi"
												 + " where med.id_usu=user.idUsu"
												 + " and secCom.idSec=med.id_sec"
												 + " and servi.idServ=med.id_serv"
												 + " and med.id_sec=:idSector "
												 + " and med.idCod=:codMedidor "
												 + " ORDER BY med.id_usu ASC");
		
		query.setParameter("idSector", idSector);
		query.setParameter("codMedidor", codMedidor);
		medi= (TblMedidor) query.getSingleResult();
		} catch (NoResultException e){
			medi=null;
		}
		return medi;
	}
	
	public TblMedidor findCodMedidor(String codMedidor){
		TblMedidor medi= new TblMedidor();
		try {
		Query query=utilFacade.getEm().createQuery("Select med from TblMedidor as med"
												 + " where med.idCod=:codMedidor");
		
		query.setParameter("codMedidor", codMedidor);
		medi= (TblMedidor) query.getSingleResult();
		} catch (NoResultException e){
			medi=null;
		}
		return medi;
	}
	

	
}
