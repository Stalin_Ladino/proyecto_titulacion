package com.ec.ups.ejb.interfac.impl;

import java.util.List;

import javax.ejb.Local;

import com.ec.ups.jpa.entidades.TblConsumo;

@Local
public interface consumoDTOInterface {
	
	void create(TblConsumo TblConsumo);

	void edit(TblConsumo TblConsumo);

	void remove(TblConsumo TblConsumo);
	
	public void modificarEstado(Integer idConsumo);

	public List<TblConsumo> buscarConsMed(String codMedidor,String fechaIni,String fechaFin);
	
	public List<TblConsumo> findAllBySector(Integer idSector);
	
	public List<TblConsumo> buscarConsMedidorFech(String fechaIni,String fechaFin,Integer idSector);
	
	
}
