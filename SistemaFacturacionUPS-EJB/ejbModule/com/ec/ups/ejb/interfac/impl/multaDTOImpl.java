package com.ec.ups.ejb.interfac.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import com.ec.ups.jpa.entidades.TblMulta;
import com.ec.ups.util.UtilConexion;

@Stateless
public class multaDTOImpl implements multaDTOInterface {

	@EJB
	private UtilConexion utilFacade;

	@Override
	public void create(TblMulta TblMulta) {
		TblMulta.setSecComDTO(null);
		utilFacade.getEm().persist(TblMulta);
	}

	@Override
	public void edit(TblMulta TblMulta) {
		TblMulta.setSecComDTO(null);
		utilFacade.getEm().merge(TblMulta);
		
	}

	@Override
	public void remove(TblMulta TblMulta) {
		utilFacade.getEm().remove(utilFacade.getEm().merge(TblMulta));
		
	}
	
//	
	public TblMulta findById(Integer id){
		TblMulta user= new TblMulta();
//		Query query2=utilFacade.getEm().createNativeQuery("select * from TBL_tipMul");
//		QUERY2.GETRESULTLIST();
		
		Query query=utilFacade.getEm().createQuery("Select tipMul from TblMulta as tipMul where idReg=:id ");
		query.setParameter("id", id);
		user= (TblMulta) query.getSingleResult();
		return user;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<TblMulta> buscarBySector(Integer idSec){
		List<TblMulta> lisMul= new ArrayList<TblMulta>();
		Query query=utilFacade.getEm().createQuery("Select tipMul from TblMulta as tipMul, secComDTO as sec"
				 								 + " where tipMul.idSec=sec.idSec"
												 + " and tipMul.idSec=:idSec"
												 + " order by tipMul.idMulta");
		
		query.setParameter("idSec", idSec);
		lisMul= query.getResultList();
		return lisMul;
	}
	
	@SuppressWarnings("unchecked")
	public List<TblMulta> findByAll(){
		List<TblMulta> lisMul= new ArrayList<TblMulta>();
		Query query=utilFacade.getEm().createQuery("Select tipMul from TblMulta as tipMul, secComDTO as sec"
												 + " where tipMul.idSec=sec.idSec"
												 + " order by tipMul.idMulta");
		
		lisMul= query.getResultList();
		return lisMul;
	}
	
	
}
