package com.ec.ups.ejb.interfac.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.ec.ups.jpa.entidades.loginDTO;
import com.ec.ups.jpa.entidades.secComDTO;
import com.ec.ups.util.UtilConexion;

@Stateless
public class loginDTOImpl implements loginDTOInterface {

	@EJB
	private UtilConexion utilFacade;

	@Override
	public void create(loginDTO loginDTO) {
		loginDTO.setIdLogin(null);
		loginDTO.setTblSectorUsuario(null);
		loginDTO.setTblUsuarios(null);
		utilFacade.getEm().persist(loginDTO);
	}

	@Override
	public void edit(loginDTO loginDTO) {
		utilFacade.getEm().merge(loginDTO);

	}

	@Override
	public void remove(loginDTO loginDTO) {
		utilFacade.getEm().remove(utilFacade.getEm().merge(loginDTO));

	}

	//
	public loginDTO findById(Integer id) {
		loginDTO rol = new loginDTO();
		Query query = utilFacade.getEm().createQuery(
				"Select rol from loginDTO as rol where idPer=:id ");
		query.setParameter("id", id);
		rol = (loginDTO) query.getSingleResult();
		return rol;
	}

	public loginDTO buscarLogin(String nombreUs, String pass) {

		loginDTO login = new loginDTO();
		try {
			Query query = utilFacade.getEm().createQuery(
					"Select login from loginDTO as login"
							+ " where login.nomUser=:nomUser"
							+ " and login.passUser=:passUser");
			query.setParameter("nomUser", nombreUs);
			query.setParameter("passUser", pass);
			login = (loginDTO) query.getSingleResult();

		} catch (NoResultException e) {
			login = null;
		}
		return login;
	}

	public loginDTO buscarLoginOperador(String nombreUs, String pass) {

		loginDTO login = new loginDTO();
		try {
			Query query = utilFacade.getEm().createQuery(
					"Select login from loginDTO as login,TblSectorUsuario as secUser"
							+ " where login.nomUser=:nomUser"
							+ " and login.passUser=:passUser"
							+ " and secUser.idLogin=login.idLogin"
							+ " and secUser.idValuenivel=3");
			query.setParameter("nomUser", nombreUs);
			query.setParameter("passUser", pass);
			login = (loginDTO) query.getSingleResult();

		} catch (NoResultException e) {
			login = null;
		}
		return login;
	}

	public List<loginDTO> findAll() {
		List<loginDTO> sect = new ArrayList<loginDTO>();
		Query query = utilFacade.getEm().createQuery(
				"Select sect from loginDTO as sect");
		sect = query.getResultList();
		return sect;
	}

	public loginDTO findByIdLogin(Integer id) {
		loginDTO rol = new loginDTO();
		Query query = utilFacade.getEm().createQuery(
				"Select login from loginDTO as login " + " where idLogin=:id ");
		query.setParameter("id", id);
		rol = (loginDTO) query.getSingleResult();
		return rol;
	}

	public loginDTO buscarLoginNombre(String nombreUs) {

		loginDTO login = new loginDTO();
		try {
			Query query = utilFacade.getEm().createQuery(
					"Select login from loginDTO as login"
							+ " where login.nomUser=:nomUser");
			query.setParameter("nomUser", nombreUs);
			login = (loginDTO) query.getSingleResult();

		} catch (NoResultException e) {
			login = null;
		}
		return login;
	}

}
