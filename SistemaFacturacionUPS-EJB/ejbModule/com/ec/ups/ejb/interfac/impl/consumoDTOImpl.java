package com.ec.ups.ejb.interfac.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.ec.ups.jpa.entidades.TblConsumo;
import com.ec.ups.jpa.entidades.TblPagina;
import com.ec.ups.util.UtilConexion;

@Stateless
public class consumoDTOImpl implements consumoDTOInterface {

	@EJB
	private UtilConexion utilFacade;

	@Override
	public void create(TblConsumo TblConsumo) {
		TblConsumo.setIdConsum(null);
		utilFacade.getEm().persist(TblConsumo);
	}

	@Override
	public void edit(TblConsumo TblConsumo) {
		utilFacade.getEm().merge(TblConsumo);
		
	}

	@Override
	public void remove(TblConsumo TblConsumo) {
		utilFacade.getEm().remove(utilFacade.getEm().merge(TblConsumo));
		
	}
	
//	
	public List<TblConsumo> buscarConsMed(String codMedidor,String fechaIni,String fechaFin){
		List<TblConsumo> cons= new ArrayList<TblConsumo>();
		try{
			
		String sql =  "SELECT * FROM tbl_consumo WHERE"
					+ " fecha_reg BETWEEN "+"'"+fechaIni+"' AND "+"'"+fechaFin+"'"
					+ " AND id_cod_med="+"'"+codMedidor+"'";
		
		
		Query query = utilFacade.getEm().createNativeQuery(sql,TblConsumo.class);
		cons= query.getResultList();
		} catch(NoResultException e){
			cons=null;
		}
		return cons;
	}
	
	public List<TblConsumo> buscarConsMedidorFech(String fechaIni,String fechaFin,Integer idSector){
		List<TblConsumo> cons= new ArrayList<TblConsumo>();
		try{
			
		String sql =  "SELECT * FROM tbl_consumo WHERE"
					+ " fecha_reg BETWEEN "+"'"+fechaIni+"' AND "+"'"+fechaFin+"'"
					+ " AND id_sec="+idSector;
		
		Query query = utilFacade.getEm().createNativeQuery(sql,TblConsumo.class);
		cons= query.getResultList();
		} catch(NoResultException e){
			cons=null;
		}
		return cons;
	}


	@Override
	public void modificarEstado(Integer idConsumo) {
		String sql ="UPDATE TblConsumo as consum SET consum.estadoFacturado='true' WHERE consum.idConsum="+idConsumo;
		Query query=utilFacade.getEm().createQuery(sql);
		query.executeUpdate();
		
	}
	
	public List<TblConsumo> findAllBySector(Integer idSector) {
		List<TblConsumo> consumoList = new ArrayList<TblConsumo>();
		Query query = utilFacade.getEm().createQuery("Select cons from TblConsumo as cons where cons.idSec=:idSector");
		query.setParameter("idSector", idSector);
		consumoList = query.getResultList();
		return consumoList;
	}

	
}
