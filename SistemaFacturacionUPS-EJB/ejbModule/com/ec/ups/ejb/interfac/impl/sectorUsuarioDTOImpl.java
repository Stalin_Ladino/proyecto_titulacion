package com.ec.ups.ejb.interfac.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import com.ec.ups.jpa.entidades.TblSectorUsuario;
import com.ec.ups.util.UtilConexion;

@Stateless
public class sectorUsuarioDTOImpl implements sectorUsuarioDTOInterface {

	@EJB
	private UtilConexion utilFacade;

	@Override
	public void create(TblSectorUsuario TblSectorUsuario) {
		TblSectorUsuario.setSecComDTO(null);
		TblSectorUsuario.setTblUsuario(null);
		TblSectorUsuario.setLoginDTO(null);
		TblSectorUsuario.setTblRol(null);
		TblSectorUsuario.setIdSecUsu(null);
		utilFacade.getEm().persist(TblSectorUsuario);
	}

	@Override
	public void edit(TblSectorUsuario TblSectorUsuario) {
		TblSectorUsuario.setSecComDTO(null);
		TblSectorUsuario.setTblUsuario(null);
		TblSectorUsuario.setLoginDTO(null);
		TblSectorUsuario.setTblRol(null);
		utilFacade.getEm().merge(TblSectorUsuario);

	}

	@Override
	public void remove(TblSectorUsuario TblSectorUsuario) {
		TblSectorUsuario.setSecComDTO(null);
		TblSectorUsuario.setTblUsuario(null);
		TblSectorUsuario.setLoginDTO(null);
		TblSectorUsuario.setTblRol(null);
		utilFacade.getEm().remove(utilFacade.getEm().merge(TblSectorUsuario));

	}
	
	
	@Override
	public void eliminarDelSec(Integer idUser,Integer idSec) {
		String sql ="DELETE FROM TblSectorUsuario as sec WHERE sec.idUsu="+idUser+" AND sec.idSec="+idSec;
		Query query=utilFacade.getEm().createQuery(sql);
		query.executeUpdate();
	}
	
	@Override
	public void eliminarDelSec(Integer idSec) {
		String sql ="DELETE FROM TblSectorUsuario as sec WHERE sec.idSec="+idSec;
		Query query=utilFacade.getEm().createQuery(sql);
		query.executeUpdate();
	}

	//
	public List<TblSectorUsuario> buscarSectorUsuario(Integer idSec) {
		List<TblSectorUsuario> lisSec = new ArrayList<TblSectorUsuario>();
		Query query = utilFacade.getEm().createQuery(
				"Select tipSec from TblSectorUsuario as tipMul,usuarioDTO as user"
						+ " where tipSec.idUsu=user.idUsu"
						+ " and tipSec.idSec=:idSec");
		query.setParameter("idSec", idSec);
		lisSec = query.getResultList();
		return lisSec;

	}

	public List<TblSectorUsuario> buscarPorCedula(String cedula) {
		List<TblSectorUsuario> lisSec = new ArrayList<TblSectorUsuario>();
		Query query = utilFacade.getEm().createQuery(
				"Select tipSec from TblSectorUsuario as tipSec,usuarioDTO as user"
						+ " where tipSec.idUsu=user.idUsu"
						+ " and user.cedUsu=:cedula");
		query.setParameter("cedula", cedula);
		lisSec = query.getResultList();
		return lisSec;
	}

	public List<TblSectorUsuario> buscarLogin(Integer idLogin) {
		List<TblSectorUsuario> listLogin = new ArrayList<TblSectorUsuario>();
		Query query = utilFacade.getEm().createQuery(
				"Select tipSec from TblSectorUsuario as tipSec,"
						+ " usuarioDTO as user," + " secComDTO as secCom"
						+ " where tipSec.idUsu=user.idUsu"
						+ " and tipSec.idLogin=:idLogin"
						+ " and secCom.idSec=tipSec.idSec");
		query.setParameter("idLogin", idLogin);
		listLogin = query.getResultList();
		return listLogin;
	}

	public List<TblSectorUsuario> findAll() {
		List<TblSectorUsuario> secList = new ArrayList<TblSectorUsuario>();
		Query query = utilFacade.getEm().createQuery(
				"Select tipSec from TblSectorUsuario as tipSec,"
						+ " rolDTO as rol,"
						+ " usuarioDTO as user," 
						+ " secComDTO as secCom"
						+ " where tipSec.idUsu=user.idUsu"
						+ " and secCom.idSec=tipSec.idSec"
						+ " and rol.idValuenivel=tipSec.idValuenivel");
		secList = query.getResultList();
		return secList;
	}
	
	public List<TblSectorUsuario> buscarUser(Integer idUser) {
		List<TblSectorUsuario> listLogin = new ArrayList<TblSectorUsuario>();
		Query query = utilFacade.getEm().createQuery(
				"Select tipSec from TblSectorUsuario as tipSec,"
						+ " usuarioDTO as user," + " secComDTO as secCom"
						+ " where tipSec.idUsu=user.idUsu"
						+ " and tipSec.idUsu=:idUser"
						+ " and secCom.idSec=tipSec.idSec");
		query.setParameter("idUser", idUser);
		listLogin = query.getResultList();
		return listLogin;
	}

	
	public List<TblSectorUsuario> findBySector(Integer idSec) {
		List<TblSectorUsuario> secList = new ArrayList<TblSectorUsuario>();
		Query query = utilFacade.getEm().createQuery(
				"Select tipSec from TblSectorUsuario as tipSec,"
						+ " rolDTO as rol,"
						+ " usuarioDTO as user," 
						+ " secComDTO as secCom"
						+ " where tipSec.idUsu=user.idUsu"
						+ " and secCom.idSec=tipSec.idSec"
						+ " and tipSec.idSec=:idSec"
						+ " and rol.idValuenivel=tipSec.idValuenivel");
		query.setParameter("idSec", idSec);
		secList = query.getResultList();
		return secList;
	}
	
}
