package com.ec.ups.ejb.interfac.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import com.ec.ups.jpa.entidades.TblEvento;
import com.ec.ups.util.UtilConexion;

@Stateless
public class eventosDTOImpl implements eventosDTOInterface {

	@EJB
	private UtilConexion utilFacade;

	@Override
	public void create(TblEvento TblEvento) {
		utilFacade.getEm().persist(TblEvento);
	}

	@Override
	public void edit(TblEvento TblEvento) {
		utilFacade.getEm().merge(TblEvento);
		
	}

	@Override
	public void remove(TblEvento TblEvento) {
		utilFacade.getEm().remove(utilFacade.getEm().merge(TblEvento));
		
	}
	
	public TblEvento findById(Integer id){
		TblEvento rol= new TblEvento();
		Query query=utilFacade.getEm().createQuery("Select rol from TblEvento as rol where idPer=:id ");
		query.setParameter("id", id);
		rol= (TblEvento) query.getSingleResult();
		return rol;
	}

	public List<TblEvento> findAll(){
		List<TblEvento> event= new ArrayList<TblEvento>();
		Query query=utilFacade.getEm().createQuery("Select event from TblEvento as event order by event.idEvento ASC");
		event= query.getResultList();
		return event;
	}
	

	
}
