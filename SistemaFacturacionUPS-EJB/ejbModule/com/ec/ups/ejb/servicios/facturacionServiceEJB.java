package com.ec.ups.ejb.servicios;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.ec.ups.ejb.interfac.impl.consumoDTOInterface;
import com.ec.ups.ejb.interfac.impl.loginDTOInterface;
import com.ec.ups.ejb.interfac.impl.medidoresDTOInterface;
import com.ec.ups.ejb.interfac.impl.usuarioDTOInterface;
import com.ec.ups.jpa.entidades.TblConsumo;
import com.ec.ups.jpa.entidades.TblMedidor;
import com.ec.ups.jpa.entidades.TblSectorUsuario;
import com.ec.ups.jpa.entidades.loginDTO;

@Stateless
public class facturacionServiceEJB{
//	private static Logger log = Logger.getLogger(SessionService.class);
	
	@EJB
	private loginDTOInterface loginDTOInterface;
	
	@EJB
	private consumoDTOInterface consumoDTOInterface;
	
	@EJB
	private usuarioDTOInterface usuarioDTOInterface;
	
	@EJB
	private medidoresDTOInterface medidoresDTOInterface;
	
	public facturacionServiceEJB() {
		
	}

	public loginDTO crearUsuario(loginDTO login)
			throws Exception {
		
		loginDTOInterface.create(login);
		return login;
	}
	
	public List<TblSectorUsuario> buscarSector(Integer idLogin)
			throws Exception {
		
		List<TblSectorUsuario> listResp= new ArrayList<TblSectorUsuario>();
	
		listResp=usuarioDTOInterface.buscarByLoginSector(idLogin);
		
		if(listResp.isEmpty()){
			listResp=null;
		}
		return listResp;
	}
	
	public loginDTO startSession(loginDTO login)
			throws Exception {
		loginDTO loginResp=new loginDTO();
		loginResp=
		loginDTOInterface.buscarLoginOperador(login.getNomUser(),login.getPassUser());
		return loginResp;
	}
	
	public TblConsumo crearConsumo(TblConsumo consumo)
			throws Exception {
		
		TblMedidor medidor=
		medidoresDTOInterface.findMedidorParaConsumo(consumo.getIdCodMed(), consumo.getIdSec());
		
		if(null!=medidor){
			Date today = new Date(); 
			Date ultimoDiaMes = this.ponerDiasFechaFinMes(today);
			Date primerDiaMes = this.ponerDiasFechaInicioMes(today);
			
			SimpleDateFormat formatoDeFecha = new SimpleDateFormat("yyyy/MM/dd");
			String primerDia= formatoDeFecha.format(primerDiaMes);
			String ultimoDia= formatoDeFecha.format(ultimoDiaMes);
			
			List<TblConsumo> resp=
			consumoDTOInterface.buscarConsMed(consumo.getIdCodMed(),primerDia,ultimoDia);
			
			if(null!=resp && !resp.isEmpty()){
				for(TblConsumo list:resp){
					consumoDTOInterface.remove(list);
				}
			}
			
			consumo.setIdMed(medidor.getIdMed());
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd"); 
		    String fechaString = formatter.format(today);
			consumo.setFechaReg(today);
			consumo.setEstadoFacturado(Boolean.FALSE);
			consumoDTOInterface.create(consumo);
			consumo.setNomUser(medidor.getTblUsuario().getNomUsu());
			consumo.setCedUser(medidor.getTblUsuario().getCedUsu());
			consumo.setFechaConsumo(fechaString);
		} else {
			consumo=null;
		}

		
		return consumo;
	}
	
	
	public Date ponerDiasFechaFinMes(Date fecha){

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha); // Configuramos la fecha que se recibe
        calendar.set(calendar.get(Calendar.YEAR),(calendar.get(Calendar.MONTH)),calendar.getActualMaximum(Calendar.DAY_OF_MONTH)); 
        return calendar.getTime(); // Devuelve el objeto Date con los nuevos d�as a�adidos

	}
	
	public Date ponerDiasFechaInicioMes(Date fecha){

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha); // Configuramos la fecha que se recibe
        calendar.set(calendar.get(Calendar.YEAR),(calendar.get(Calendar.MONTH)),calendar.getActualMinimum(Calendar.DAY_OF_MONTH)); 
        return calendar.getTime(); // Devuelve el objeto Date con los nuevos d�as a�adidos

	}
	
	
	public loginDTO eliminarUsuario(loginDTO login)
			throws Exception {
		
//		loginDTOInterface.delete(login);
		return login;
	}

	
}
